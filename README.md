# Animations WordPress organisées par le Pic.org

Ce dépôt propose les notes et ressources utilisées lors des animations relatives à WordPress proposées par l'association membre du collectif C.H.A.T.O.N.S le-pic.org.

Liste des animations :

1. [WordPress Prise en main](../blob/master/Notes%20animation%20WP%20Prise%20en%20main%20du%20Pic.md)

  Avec plus de 43% des sites qui l'utilise en 2024, WordPress est l'un des CMS (Content Managment System ou  Système de gestion de contenu) les plus utilisé au monde. WordPress a une part de marché de 62,8% sur le marché CMS.
  Gratuit, modulaire, mené par une vaste communauté, WordPress est maintenant devenu un outil incontournable qui a largement fait ses preuves.
  Nous allons vous montrer qu'avec cet outil, créer votre site web associatif, n'a rien de bien compliqué ...

  **Objectif** : Détailler la rédaction d'articles et organiser le contenu, tout en prenant en main la présentation et les fonctionnalités du site sous WordPress

  **Pré-requis** : Posséder les notions de fichiers et de répertoires, savoir utiliser un navigateur Internet.

  **Matériel** : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.

  **Logiciels nécessaires** - Si vous apportez votre machine, merci de les installer avant le début de la session pour réserver un maximum de temps au monitorat :

 * Le navigateur Firefox ou Chromium.

  **Présentation PDF** : [Présentation WordPress prise en main](../blob/master/Présentations/PrésentationWPRédacteur.pdf)

2. [Personnaliser son site sous WordPress](../blob/master/Notes%20animation%20personnaliser%20WordPress.md)

Si vous avez installé un thème WordPress mais qu’il n’est pas tout à fait approprié pour vous, vous vous sentez peut-être frustré. Il y a beaucoup d’options disponibles pour personnaliser votre thème WordPress.

Vous savez comment écrire des pages et des articles dans un site wordpress ? Peut-être avez-vous suivi notre atelier [WordPress Prise en main](../blob/master/Notes%20animation%20WP%20Prise%20en%20main%20du%20Pic.md)

Vous voudrez aller plus loin dans la maîtrise de votre site: comment créer des menus ? Comment mettre des widgets sur vos pages ? D'ailleurs c'est quoi un widget ? 

Vous voudrez changer de manière drastique la présentation de votre site ? Pour cela il faut s'intéresser aux "thèmes" de Wordpress.

Tous ces sujets seront abordés au cours de cet atelier, ponctué de travaux pratiques.

Dans cette animation, nous allons vous guider à travers les options de personnalisation du thème WordPress, vous aider à choisir celui qui vous convient et vous montrer comment le faire de manière sûre et efficace. Nous verrons également comment ajouter de nouvelles fonctionnalités au site grâce aux Widget et aux plugin.

Nous ferons enfin unu introduction sur la personnalisation d'un thème avec les CSS.

  **Objectif** : Savoir personnaliser un site élaboré avec WordPress.

  **Pré-requis** : Avoir déjà travaillé avec WordPress et connaître les CSS, ou avoir suivi les ateliers sur Wordpress et sur les CSS

  **Matériel** : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.

  **Logiciels nécessaires** - Si vous apportez votre machine, merci de les installer avant le début de la session pour réserver un maximum de temps au monitorat :

 * Le navigateur Firefox ou Chromium ;
 * Un éditeur de texte (mais pas un traitement de texte) : par exemple Notepad++ sous Windows, ou Geany sous Gnu/Linux.

  **Présentation PDF** : [Présentation personnaliser son site WordPress](../blob/master/Présentations/PrésentationWPPersonnalisation.pdf)

3. [Personnaliser le thème visuel de son site sous WordPress](../blob/master/Notes%20animation%20personnaliser%20WordPress%20niv.2.md) 

Vous avez choisi un thème pour votre site Wordpress (vous savez donc déjà changé l’apparence de votre site, peut-être avez-vous suivi notre atelier [WordPress Prise en main](../blob/master/Notes%20animation%20WP%20Prise%20en%20main%20du%20Pic.md) ?)

Mais vous n’êtes pas encore satisfaits : vous souhaitez modifier légèrement un aspect de votre site.

Aprés avoir vu comment ajouter des fonctionnalités à son site sous WordPress, comment choisir un thème et l'adapter avec les outils installés par défaut avec WordPress, nous allons voir comment modifier plus en profondeur un thème ou en créer un complétement original.

Cette animation se déroulera en deux parties :

1.Personnalisation d'un site WordPress avec le constructeur de site Elementor ;
2.Exemple de création d'un thème personnalisé avec HTML, CSS et PHP.

**Objectif** : Pour ceux qui connaissent WordPress et qui veulent complétement modifier la présentation de la partie visible de leur site. L'idéal étant d'avoir suivi les animations [WordPress Prise en main](../blob/master/Notes%20animation%20WP%20Prise%20en%20main%20du%20Pic.md) et [Personnaliser son site sous WordPress](../blob/master/Notes%20animation%20personnaliser%20WordPress.md), cependant une connaissance des fonctionnalités principales de WordPress est suffisante.

**Pré-requis** : un minimum de connaissances de WordPres.

**Matériel** : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.

**Logiciels nécessaires** - Si vous apportez votre machine, merci de les installer avant le début de la session pour réserver un maximum de temps au monitorat :

 * Le navigateur Firefox ou Chromium ;

Les présentes formations sont protégées par la [licence CC](../master/LICENSE.md)
