# Nouveautés WP 6.2

## Editeur de contenu
- Ajout d'un onglet à droite permettant d'accéder à des paramétrages de configuration des styles pour certains blocs (pas les paragraphes par exemple)
- Ajout d'une fonction destinée à copier les styles (dans le menu contextuel du bloc)
- Ajout du mode "sans distraction" dans le menu de droite de WP (en haut) 
- Le menu des compositions à gauche est maintenant structué par catégories
- Au même endroit un nouveau menu "Média" est ajouté, permettant d'ajouter une image de la bibliothèque directement sans passer par le bloc image puis ajout d'une image
- Egalement dans ce menu, ajout d'un accès à Openverse (projet Wordpress) : Projet d'images libres de droits
- En plus de la vue en liste du document, il y a aussi maintenant une vue en Structure (bloc HTML)
 

## Editeur de site
- Fin de la phase 2 du projet Guntherberg. L'éditeur n'est plus en version béta.
- Il faut toujours utiliser un theme compatible FSE (Full Site Editing)
- Modification des modèles et des éléments de modèle.
- On peut ajouter du CSS personnalisé : Menu style puis CSS (possible pour tout le site ou par type de blocs)
- Dans le menu de droite l'oeil permet d'accéder au Guide style : permet d'homogénéiser son site
- Ajout de la fonction de mise en évidence du bloc en cours de personnalisation
- La bloc navigation a été refondu dans la version 6.2. Il est maintenant beaucoup plus exploitable dans la version 6.1 où il était très complexe à utiliser. Il est possible d'importer des menus crée à partir d'une ancienne version de WP mais également des Widgets.


# Nouveautés WP 6.3

## Editeur de contenu

- Arrivée de nouveaux blocs dont :
	- notes en bas de de page
	- détail : permet de créer une section basculante (pour masquer ou afficher du contenu)	
- L'outil de configuration des marges a été revu : il est plus visuel
- Au niveau des images, il y a maintenant un outil de configuration de la proportion qui permet d'appliquer des formats prédéfinis
- Nouveautés du bloc bagnière :
	- la configuration des styles est héritée pour tous les blocs internes de la bagnière.
	- il est possible d'ajouter un effet duo tone 
	- L'option "les blocs intérieurs utilisent la largeur de contenu" permet de limiter la largeur des blocs contenus dans la bagnière.
- La création des liens a été modifiée : il faut cliquer deux fois pour accéder à la configuration du lien (par exemple ouvrir dans un nouvel onglet).
- Le terme de blocs réutilisables disparait pour être remplacé par celui de compositions synchronisées. Ces compositions apparaissent dans le sélecteur de blocs à gauche (icone completement à droite proche de médias).

## Editeur de site

Trois nouvelles options de configuration :

- Navigation
	-> entièrement configurable depuis le menu de l'éditeur de site.	

- Les palettes de commandes : En cliquant sur la loupe en haut, celà permet de nanviguer rapidement sur toutes les pages de l'éditeur de site. C'est dispo aussi depuis l'éditeur de contenu avec CTRL+K.

- Style
	-> une autre entrée vers les fonction de gestion des styles déjà disponibles dans la versions 6.1
	-> les révisions sont maintenant disponibles avec les styles

- Page : les pages sont éditables directement depuis l'éditeur de site. Celà va devenir la norme et à terme on aura plus besoin de passer par l'éditeur de contenu.

- Les éléments de modèle sont remplacés par les Compositions.

# Nouveautés WP 6.4

La grande nouveauté est le thème 20.24

- Pas mal de choses non visibles (optimisations).

- Quelques fonctionnalitées repoussées : 
	- la gestion des polices sera intégrée dans une prochaine version (6.5 ?)
	- Table des matières ;
	- Marquage off scroll (texte défilant) ;
	- Temps de lecture estimé.

- Sur les images une nouvelle fonction apparait : déplier au clic, qui permet d'associer une lightbox à l'image et l'agrandie lors d'un clic sur l'image.

- Au niveau de la vue liste des blocs, il est possible maintenant de renommer les groupes.

- Il est possible maintenant de configurer le style pour un ensemble de blocs en même temps (avant il fallait faire bloc par bloc).

- Il est possible de personnaliser un peu plus le nouveau bloc, note de bas de page.

- De même pour le nouveau bloc détails, il est maintenant possible de modifier les marges.

- De nouvelles commandes ont été ajoutées à la palette de commande.

- L'accès et les filtres des compositions ont été améliorés dans les deux éditeurs (contenu et site).

- Il est possible d'exporter ses compositions ou d'importer des compositions externes.

# Perspectives de Gunthenberg

Phase 3 : mise en place de la collaboration en 2024 (comme Google Docs)

Phase 4 : mise en place du multi lingue en 2025-2026

Et des mises à jour du reste.

En 2024 3 nouvelles version

Mars : 6.5 : première version de la phase 3
Juillet : 6.6 : maintenance et optimisation des performances
Novembre : 6.7 : nouveau ajouts de la phase 3





