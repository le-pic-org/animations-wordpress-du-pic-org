# Notes animation prise en main de WordPress

Avec plus de 43% des sites qui l'utilise en 2024, WordPress est l'un des CMS (Content Managment System ou  Système de gestion de contenu) les plus utilisé au monde. WordPress a une part de marché de 62,8% sur le marché CMS.
Gratuit, modulaire, mené par une vaste communauté, WordPress est maintenant devenu un outil incontournable qui a largement fait ses preuves.
Nous allons vous montrer qu'avec cet outil, créer votre site web associatif, n'a rien de bien compliqué ...

  **Objectif** : Détailler la rédaction d'articles et organiser le contenu, tout en prenant en main la présentation et les fonctionnalités du site sous WordPress

  **Pré-requis** : Posséder les notions de fichiers et de répertoires, savoir utiliser un navigateur Internet.

  **Matériel** : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.

  **Logiciels nécessaires** - Si vous apportez votre machine, merci de les installer avant le début de la session pour réserver un maximum de temps au monitorat :

 * Le navigateur Firefox ou Chromium.

  **Présentation PDF** : [Présentation WordPress prise en main](../blob/master/Présentations/PrésentationWPRédacteur.pdf)

## Remarque préliminaire : Nouvelle version de WordPress 6.7.1 (décembre 2024)

Le projet Gutenberg de WordPress est divisé en quatre phases distinctes, chacune visant à améliorer et à étendre les fonctionnalités de l'éditeur de blocs. Voici un aperçu des quatre phases :

    Phase 1 - Easier Editing (Édition plus facile) : achevée
        Introduction du nouvel éditeur de blocs (Gutenberg) avec WordPress 5.0 en 2018.
        Intégration du glisser-déposer pour une expérience d'édition plus intuitive.
        Chaque élément de contenu est traité comme un bloc, permettant une édition et une création de contenu plus dynamiques.

    Phase 2 - Customization and Gutenberg (Personnalisation et Gutenberg) : achevée
        Concentration sur des fonctionnalités de personnalisation avancées allant au-delà des blogs et des articles.
        Introduction de l'édition complète du site (Full Site Editing) avec des blocs étendus pour la mise en page avec WordPress 5.8 en 2021.
        Création de modèles de blocs prédéfinis (Block Patterns) pour une édition plus facile.
        Mise en place d'un répertoire de blocs (Block Directory) pour des extensions et plugins basés sur les blocs.

    Phase 3 - Collaboration (Collaboration) : en cours
        Intégration de fonctionnalités de co-édition pour permettre à plusieurs auteurs de travailler simultanément sur un même document.
        Inspiré par les fonctionnalités de collaboration de Google Docs, cette phase vise à améliorer la collaboration dans l'édition de contenu WordPress.

    Phase 4 - Multilingual (Multilingue) : à venir
        Accent mis sur la prise en charge multilingue pour permettre aux utilisateurs de développer des sites Web dans différentes langues.
        L'objectif est de fournir une prise en charge officielle de la traduction pour les sites, réduisant ainsi la dépendance des plugins de traduction externes.

Ces phases témoignent de l'évolution continue du projet Gutenberg, visant à transformer l'expérience d'édition sur WordPress et à élargir les fonctionnalités de l'éditeur de blocs au-delà des publications de blog.

Sur la version 5 (fin 2017), la nouveauté était le nouveau éditeur Gutenberg. Il fonctionne sur le principe des blocs.
Tout semble, en effet, bien plus intuitif, car les blocs peuvent être déplacés un peu partout et les éléments sont clairement distincts. Il reste toujours possible d’utiliser l’éditeur classique à la place de ce nouvel éditeur visuel mais plus les versions avancent à maturité, plus il semble préférable d'utiliser Gutenberg. On perçoit toutefois la volonté des responsables de WordPress de continuer de faire de ce CMS un outil de construction de sites, accessible à tous. Il est possible de revenir à l'ancien mode d'édition avec le plugin classic éditeur, mais attention risque de problèmes si on passe du nouveau vers l'ancien mode d'édition.
D'une manière générale le nouvel éditeur est davantage adapté pour les nouveaux venus à WordPress.

En juillet 2021 avec la version 5.8, apparait le Full Site Editing (FSE). WordPress tend à s’adresser à un plus grand groupe d’utilisateurs qui ne savent pas forcément coder. WordPress simplifie progressivement le processus de création de sites web avec des opérations simples de glisser-déposer et sans toucher au code, afin que tout le monde puisse l’utiliser. Il existait un certain nombre d’obstacles à la personnalisation, qu’il s’agisse de petites modifications sur des blocs séparés ou sur le site. Désormais, le FSE aide les utilisateurs de WordPress à créer des mises en page d’une manière beaucoup plus souple et pratique.

En novembre 2022 sort la version 6.1, équipée du nouveau thème Twenty Twenty-Three (TT3), livré avec 10 variantes conçues par les membres de la communauté WordPress.
Cette version offre une meilleure expérience de création grâce à de nouveaux modèles, plus de contrôle sur les outils de conception, une simplification de la création et de la modification des menus, une amélioration de la visualisation des réglages des publications et bien d'autres choses.

Les mises à jour de WordPress (WP) 6.2, 6.3 et 6.4 apportent des améliorations significatives à l'éditeur de contenu et de site. Dans la version 6.2, l'éditeur de contenu introduit un onglet de configuration des styles pour certains blocs, une fonction de copie de styles, le mode "sans distraction", une structure catégorisée pour les compositions, et un accès direct aux médias, y compris Openverse. Une nouvelle vue en Structure (bloc HTML) est ajoutée. L'éditeur de site marque la fin de la phase 2 du projet Guntherberg, avec une nécessité de thèmes compatibles FSE (Full Site Editing) et la possibilité d'ajouter du CSS personnalisé.

La version 6.3 de l'éditeur de contenu présente de nouveaux blocs tels que les notes en bas de page et les détails, une configuration visuelle des marges, un outil de proportion pour les images, et des améliorations du bloc bannière. Les liens nécessitent désormais un double-clic pour la configuration. Les blocs réutilisables sont remplacés par des compositions synchronisées.

Le site 6.3 propose trois nouvelles options : la configuration complète de la navigation depuis le menu de l'éditeur de site (FSE), des palettes de commandes pour naviguer rapidement, et un accès amélioré aux styles. Les pages deviennent éditables directement depuis l'éditeur de site, et les éléments de modèle sont remplacés par des compositions.

A partir de 2024, la version 6.4 introduit le thème Twenty Twenty-Four (20.24 - (TT4)), livré avec 10 variantes conçues par les membres de la communauté WordPress, avec des optimisations. Certaines fonctionnalités sont reportées, mais de nouvelles options apparaissent, comme "déplier au clic" pour les images, le renommage des groupes de blocs, la personnalisation groupée des styles, et des améliorations dans l'accès et les filtres des compositions, avec la possibilité d'exporter/importer ces compositions.

La version 6.5 se concentre sur la personnalisation et l'expérience utilisateur. Elle introduit une gestion des polices Google via une bibliothèque dédiée, un historique des révisions des styles, et des outils avancés pour la mise en page (fonds, ratios, ombres). Les développeurs bénéficient de nouvelles API pour créer des interactions et lier des blocs. Enfin, les dépendances de plugins sont mieux gérées, et les thèmes classiques reçoivent de nouvelles options d'apparence sans nécessiter de fichiers supplémentaires.

Avec la version 6.6, la personnalisation atteint un nouveau niveau grâce à des palettes de couleurs enrichies et des choix de polices supplémentaires. Les utilisateurs profitent d’aperçus rapides des pages pour une édition plus fluide, et une fonction de restauration permet de revenir à une version précédente en cas de problème avec une mise à jour automatique des plugins. Les performances et l'accessibilité continuent d’être optimisées.

Fin 2024, la version 6.7 marque l’arrivée du thème Twenty Twenty-Five avec des designs modernes et personnalisables. Une nouvelle fonction "Zoom Out" offre une vue d’ensemble du site pour une meilleure navigation. Les utilisateurs peuvent désormais ajouter des champs personnalisés aux blocs, appliquer des préréglages de tailles de police, et profiter du support natif des images HEIC. Cette version se concentre aussi sur l'amélioration continue des performances et de l'accessibilité.

Au Pic, nous installons la dernière version stable de la branche courante de WordPress (Version 6.7.1, publiée le 21 décembre 2024).

Remarque : pour les anciens thèmes non compatibles avec le FSE, WordPress utilisera les anciens mode de mise en forme et de configuration du site (Menus, Widgets). Même si de plus en plus de thèmes compatibles FSE sont disponibles, un nombre important de sites non compatibles sont en production. Il nous semble donc encore utile d'accompagner les auditeurs à savoir utiliser les anciennes fonctionnalités. Cependant nous ne verrons pas la création de contenue avec Classic editor qui est maintenant totalement obsolète.

Le FSE s'appelle maintenant l'éditeur de site alors que l'éditeur des articles et des pages s'appelle l'éditeur de contenu.

## Plan présentation

1. Qu'est ce que WordPress ?
2. Découverte rapide de la partie Administration
3. Les articles (posts)
4. Les catégories et étiquettes
5. L'éditeur de texte
6. La gestion des images
7. Intégrer des éléments externes (réseaux sociaux, vidéos)
8. Les Menus
9. Les pages
10. La gestion des utilisateurs
11. Les commentaires

## 1 - Qu'est ce que WordPress ?

WordPress est un *CMS*. *Content Managment System*. Initialement dédié à la création de blogs.
Permet d'éviter de développer la partie visible d'un site. Cache la partie du site qui permet la gestion de la partie visible. Il permet la séparation du contenu et de la présentation.

En 2024, les systèmes de gestion de contenu (CMS) continuent de jouer un rôle central dans la création et la gestion de sites web. Voici un aperçu des parts de marché des principaux CMS cette année :

### **Part des CMS parmi les sites web :**
- **WordPress :**  
  - Part de marché des CMS : **62,8%**.  
  - Proportion de tous les sites web : **43,1%**.  

- **Shopify :**  
  - Part de marché des CMS : **6,2%**.  
  - Proportion de tous les sites web : **4,2%**.  

- **Wix :**  
  - Part de marché des CMS : **3,8%**.  
  - Proportion de tous les sites web : **2,6%**.  

- **Squarespace :**  
  - Part de marché des CMS : **3,0%**.  
  - Proportion de tous les sites web : **2,1%**.  

- **Joomla :**  
  - Part de marché des CMS : **2,5%**.  
  - Proportion de tous les sites web : **1,7%**.  

- **Drupal :**  
  - Part de marché des CMS : **1,5%**.  
  - Proportion de tous les sites web : **1,1%**.  

### **Analyse :**
WordPress reste le leader incontesté des CMS avec plus de 60% de part de marché, alimentant plus de 43% des sites web dans le monde. Cependant, d'autres plateformes comme Shopify et Wix continuent de gagner en popularité, notamment dans le commerce électronique et pour les utilisateurs recherchant des solutions simples à utiliser.

---

### **Sources :**
- [Part de marché des CMS 2024 : tendances et statistiques d’utilisation](https://www.wpade.com/fr/cms-market-share-1.html)
- [Les Parts de Marché des CMS en 2024](https://group-easymarketing.com/2024/08/05/les-parts-de-marche-des-cms-en-2024/)
- [Les parts de marché des CMS en 2024](https://blog.osiom.fr/les-parts-de-marche-des-cms-en-2024/)

Popularité = quelqu'un qui ne sais pas coder peut créer un site web

Pour les développeurs web, WordPress est un gain de temps énorme, pour des projets simples et ne nécéssite pas d'avoir de compétences en design, grace aux thèmes (un design) existants.

L'infrastructure est très modulaire (thèmes, plugin, widget).

Bien faire la différence entre les deux sites dédiés à WordPress :
1. *WordPress.com*
2. *WordPress.org*

*WordPress.com* est une plateforme de mise en ligne de sites basées sur WordPress. Choix du thème, etc ... Tout est géré automatiquement.

*WordPress.org* est la plateforme de la communauté. On peut y récupérer le logiciel. Mais il nous incombe de mettre en place l'outil quelque part, manuellement ou de déléguer l'installation à un spécialiste. Il est plus facile de le personnaliser. Un exemple d'hébergement, le PIC !
De plus, dans le cas d'une installation personnalisé et manuelle, le site nous appartient, alors que dans le cas d'une installation en utilisant WordPress.com, c'est comme Facebook, le site ne nous appartient pas. On ne maitrise pas le fait que l'hebergeur puisse placer par exemple de la pub sur notre site.

Les Options disponibles dans le cas d'une installation via WordPress.com sont limités (choix de template limité), alors qu'avec WordPress.org, la communauté propose un grand nombre de modules (thèmes, plugin, etc).

## 2 - Découverte rapide de la partie Administration et préparation des instances

La partie administration permet de gérer le contenu de la partie visible du site.

On accède à la partie administration en ajoutant wp-admin à l'URL du site. On est automatiquement redirigé vers la page de connexion du site (wp-login).

A noter qu'une fois connecté, une barre d'action s'ajoute en haut du site publique. Elle permet de basculer de la partie admin à la partie visible.

**Démontration** : 	
* montrer comment modifier le slogan du site, et la langue. (Indiquer au passage que c'est facile de passer du francais à l'anglais et inversement pour par exemple trouver la correspondance des termes pour rechercher des informations sur le net) ;
* création des comptes utilisateurs sur chaque instance ;
* montrer comment faire pour changer un thème (basculer sur Twenty Twenty-Four), faire faire la manip. sur chaque instance.

**Conclusion** : l'idée de cette partie est de montrer que le site est constitué de deux parties, et qu'il facile de passer de l'une à l'autre.

## 3 - Les articles (posts)

Articles = Posts (en anglais)

**Remarque** : noter la différence entre une page et un article. Une page est dédié au contenu fixe. Par exemple la page contacts, à propos, présentation des services, etc ...
Au contraire pour le contenu qui doit évoluer au fil du temps, il est préférable d'utiliser les articles. Par exemple, le compte rendu de la dernière AG, la dernière formation etc ...

Les articles correspondent au contenu en lien avec l'actualité. Pour le comprendre, il faut revenir à l'historique de WordPress, qui à l'origine était dédié aux blogs. L'unité dans un blog est l'article (le Post).

**Démontration** : 	

	Cliquer sur l'entrée du menu Articles. Noter que par défaut, il y a pas d'article.

	Création d'un article :

		Cliquer sur Ajouter un nouvel article.
			Le l'instance WordPress demande un titre et un contenu. Pour le test, copier du contenu depuis le site du PIC par exemple. On peut remarquer que le formatage est bien copié entre la page du site originel et le contenu dans WordPress.

		Cliquer sur Publier.
		Si on retourne sur le site visible, on voit que présent imédiatement sur le site.
		Si on clique sur le lien correspond du Post (site visible), on accède à l'article complet.
		Remarque : si on observe l'URL dans la barre d'adresse du navigateur on constate qu'elle n'est pas très belle. On peut changer la configuration des Permaliens (permalink) :

		----  	/!\ attention a ne pas faire faire aux stagiaires lors de l'animation pour éviter tout problème sur le site école !		

				C'est dans Réglages > Permaliens. Par défaut le réglage est sur "Structure personnalisée". Pour améliorer le référencement (SEO) par les moteurs de recherche, on place le réglage sur "Nom de l'article" ou "Titre de la publication" (version 5).
					On clique sur "Enregister les modifications".
					On retourne ensuite dans la partie visible pour afficher la barre d'URL après avoir rafraichi la page.
					A noter qu'il est possible de changer manuellement le permalien en retournant dans l'artiche dans la partie administrateur, juste en dessous du titre de l'article.
		----				
		Il est possible aussi d'utiliser le bouton prévisualiser pour afficher directement le lien du post dans la partie visible du site.

		Ajouter plusieurs articles (le faire faire par les stagiaires sur le site école). Puis retourner sur la partie visible du site. Sur la page d'accueil faire constater que l'on peut voir un résumé de toutes les articles. Chaque article a sa propre URL.

	Suppression d'un article :

		On supprime l'article pour montrer comment faire.
		Basculer sur la partie visible pour montrer que l'on a "Aucun résultat".

		TODO : faire-faire la manip. de suppression d'un article aux stagiaires

**Conclusion** : Il est donc facile de créer du contenu avec les articles. Nous allons voir comment les catégoriser.

## 4 - Les catégories et étiquettes (tags)

### Les catégories

Ces deux éléments servent à catégoriser le contenu du site.

Ceci permet par exemple de sélectionner un certain type de contenu qui n'interesse pas forcément tous les visiteurs.

Démonstration :

	On retourne sur la partie privée du site. On clique sur "tous les articles". On retrouve tous les articles saisies par les stagiaires.
	On va créer plusieurs catégories : astuces, etc...
	On trouve sur la droite de l'article (en mode modification), une case à cocher "Non classé" dans une zone "Catégories". On trouve un lien en dessous "Ajouter une nouvelle catégorie". On clique sur ce lien.
	Le lien se transforme en zone de saisie. On peut y saisir le libellé de la catégorie. On saisie par exemple "Astuces" (ou autre si un article fait référence à un thème précis).
	On clique sur le bouton "Ajouter une nouvelle catégorie".
	On décoche ensuite la catégorie "Non classé", puisque l'article fait maintenant partie d'une catégorie.
	On clique sur "Mettre à jour".

	Remarque : ce qui a été fait sur l'article aurait aussi pu être fait en cliquant sur l'entrée du menu "Catégories".

	Si on revient maintenant dans "Tous les articles", on peut voir dans la liste que la catégorie de l'article modifié a été mise à jour.

	Remarque : depuis cette vue, il est possible de modifier directement les catégories, ce qui évite d'entrée dans le mode de modification de chaque article. On clique pour cela sur le lien "Modification rapide", sous le titre de l'article. Les catégories ayant été au préalable créées, il est maintenant possible de les cocher avec le mode "modification rapide". Cliquer ensuite sur "Mettre à jour".

	On clique maintenant sur l'entrée du menu "Catégories". On va gérer les catégories et les sous-catégories. On crée une nouvelle catégorie dans laquelle on modifie la catégorie Parente (liste "Parent"). Puis on clique sur le bouton "Ajouter une nouvelle catégorie". Si on observe la zone de droite, la nouvelle sous-catégorie d'affiche bien sous la catégorie parente.

	Si on retourne dans la liste des articles (menu "Tous les articles"), puis que l'on clique sur "Modification rapide", la liste des catégories, contient bien maintenant la nouvelle sous-catégorie.

### En ce qui concerne les étiquettes (tags).

Tout comme les catégories, les tags sont utilisés pour facilité l'indexation par les moteurs de recherche.

**Démontration** :

	Sur la partie droite d'un article en cours de modification, on trouve une zone "étiquette". Pour le contenu de ce champ, on va se demander ce qu'un visiteur du site devrait taper pour trouver l'article. Exemples : formation en ligne, WordPress, CSM, etc ...

	Puis on clique sur "Ajouter". On le voit apparaître juste en dessous.
	Penser à cliquer sur le bouton "Mettre à jour" de l'article.

	Si on retourne sur le menu "Tous les articles", on apercoit que la nouvelle étiquette a bien été ajoutée à l'article concerné. Si on clique sur "Modification rapide",
	il est possible d'ajouter ici aussi une nouvelle étiquette à un autre article.

	Remarque : il exite comme pour les catégories une entrée de menu spécifique pour les étiquettes, désigné par "Etiquettes".
	Remarque : il est possible de placer plusieurs étiquettes sur un même post, en plaçant une virgule entre chaque mot clef.

	On retourne maintenant sur la partie visible du site. Sur la page d'accueil, faire remarquer l'apparition des catégories et des étiquettes devant chaque article.

	Chaque catégorie et étiquette est un lien sur lequel on peut cliquer. En cliquant que une catégorie, on se place dans la liste des articles faissant parties de cette catégorie.
	Il est possible de faire la même chose avec les étiquettes.

**Conclusion** : Les catégories et les étiquettes permettent de faciliter la navigation dans le site web et notamment pour les gros sites web. Cette organisation aide pour le *SEO*. Nous allons voir maintenant plus en détail l'éditeur d'article.

## 5 - L'éditeur de texte

------
EDIT 17 décembre 2020 : Le texte ci dessous fait référence à l'ancien éditeur de texte de WordPress. Initialement conservé pour mémoire au départ de la mise en place de Gutenberg, l'ancien éditeur est maintenant totalement désué.  

L'éditeur de texte standard de WordPress dispose de toutes les fonctionnalités standards d'un outil d'édition : entre autres, changement en gras, italique, barré, etc.
Il est possible d'ajouter des listes, numérotées ou pas.
Le bloc de citation (bloc cote) permet de placer une partie du texte d'un article dans un style particulier. Ceci permet de mettre un peu de relief dans un article et en particulier le contenu d'un autre auteur que l'on veut mettre en avant.
La ligne horizontale permet de séparer du texte.
On trouve également des fonctions d'alignement du texte (à gauche, au centre, à droite).

On trouve enfin les boutons permettant de créer des liens (URL). En sélectionnant une partie du texte, puis en cliquant sur l'icone "lien", une fenêtre de saisie de l'URL du lien va s'ouvrir. Si on connait l'URL, il suffit dans la coller dans cette zone. Dans le cas contraire, dans le cas d'un renvoit vers une page interne du site, il est possible de cliquer sur l'icone en forme de roue. Cela affiche une fenêtre plus complète permettant de recherche la page adéquate. Bien entendu il est possible de placer un lien vers une page externe.

Si on repasse avec le pointeur de la souris sur le lien précédemment créé, une petite zone s'affiche permettant de modifier le lien ou le retirer. l'icone suivante dans la barre permet de retirer directement un lien.

-----

C'est maintenant l'éditeur au format blocs Gutenberg qui est la référence depuis la version 5 de WordPress. Il porte maintenant le nom d'éiteur de contenu.

Les développements de l'éditeur de contenu Gutenberg sont très actifs. Il est donc possible que certaines fonctionnalités évoluent rapidement. Lorsque les fonctionnalités de Gutenberg sont jugées suffisamment stables, elles sont intégrées dans la version de WordPress.

A noter que la tendance de ce CMS est de commencer à pouvoir mettre à jour le contenu depuis la partie éditeur de site. Ceci est possible depuis la version 6.3. Nous verrons cela plus tard dans l'animation "personnaliser son site WordPress". Cependant, certaines perspectives laissent penser que l'éditeur de contenu va disparaitre à terme.

Il existe beaucoup de blocs différents dans le nouvel éditeur de WordPress.
Voici une liste des blocs de WordPress :

1. Blocs de texte :
    - Paragraphe
    - Titre
    - Bouton
    - Liste
    - Citation
    - Citation en exergue

2. Blocs de média :
    - Image
    - Son
    - Bannière
    - Vidéo
    - Galerie
    - Fichier
    - Embed (https://youtu.be/ouvTudXsNtM)

3. Blocs de mise en page :
    - Colonnes
    - Média texte
    - Séparateur
    - Espacement

4. Blocs de code

5. Blocs ajoutés dans la version 6.2 :
    - Notes en bas de page
    - Détails

Notez que chaque bloc a ses propres options de configuration et de style.

**Remarque** : Avec le thème par défaut de WordPress, ce que l'on voit dans l'éditeur de texte est très sensiblement la même chose que l'on voit dans le site publique. Ceci n'est pas forcément vrai pour tous les thèmes.

WordPress dispose d'un répertoire de blocs. Pour ouvrir le répertoire de blocs dans WordPress, suivez ces étapes :

1. Connectez-vous à votre tableau de bord WordPress.
2. Naviguez vers un article ou une page que vous souhaitez modifier.
3. Cliquez sur le bouton "Modifier" pour ouvrir l'éditeur de blocs Gutenberg.
4. Cliquez sur le bouton "+" en haut à gauche de l'éditeur pour ouvrir le répertoire de blocs.
5. Vous pouvez maintenant parcourir les blocs disponibles et les ajouter à votre contenu.
6. Notez que le répertoire de blocs est organisé en différentes catégories pour faciliter la recherche de blocs spécifiques. 

**Remarque** : Vous pouvez également utiliser la barre de recherche en haut du répertoire pour trouver rapidement un bloc par son nom. La combinaison de touches CTRL + K permet également d'accéder à la barre de recherche. Cette fonction a été enrichie dans les dernières versions de WordPress. Cela permet progressisvement de réduire l'utilisation de la souris en utilisant les raccourcis clavier et les suggestions de fonctionnalités que propose cette barre de recherche.

**Démonstration** : Montrer comment activer certaines fonctions via la commande CTRL + K.

Depuis la version 6.2 de WordPress, de nouveaux blocs ont été ajoutés, tels que les "notes en bas de page" et les "détails" permettant la création de sections basculantes. Ces blocs offrent des fonctionnalités améliorées pour configurer les marges, ajuster la proportion des images, et introduisent des options avancées pour le bloc bannière, notamment la configuration des styles hérités, l'ajout d'effets duotone, et la limitation de la largeur des blocs. De plus, les liens ont subi des modifications nécessitant désormais un double-clic pour accéder à leur configuration, et le concept de "blocs réutilisables" a été remplacé par les "compositions synchronisées".

WordPress propose également des modèles de blocs. Il s'agit de blocs préconfigurés. Par exemple, un bloc de texte avec une image à gauche, un titre, un paragraphe, etc. Il est possible de créer ses propres modèles de blocs. On parle maintenant de compositions de blocs. C'est une fonctionnalité qui a été ajoutée dans la version 6.3 de WordPress. 

**Démonstration** : Montrer comment créer une composition de blocs et les gérer.

Il est possible de prévisualiser les modifications sans avoir besoin de sauvegarder.

**Démonstration** : En mode modification dans un article, dans l'espace privé du site, appuyer sur le bouton "Prévisualiser les modifications". Comparer avec l'article en mode publique avant la sauvegarde. Montrer les modifications des différentes options de l'éditeur.

**Remarque** : lorsque on sauvegarde les modifications, une petite roue figure l'avancement de la sauvegarde, ce qui rappel que le site n'est pas en local mais en ligne.

La balise "lire la suite", permet de choisir un endroit personnalisé afin que sur le site publique, en mode page d'accueil, le résumé des articles s'arrête spécifiquement à l'endroit où nous avons mis en place la balise "lire la suite". Il s'agit de l'extrait qui sera affiché sur la page d'accueil.

**Démontration** : faire la démo de cette fonction.

Le dernier bouton de la barre permet d'afficher des fonctions d'édition complémentaires. Noter que lorsqu'on clic dessus, de nouvelles icones apparaissent en dessous des premières.

Par exemple la liste "Paragraphe" permet de mettre en forme selon les différentes type possible de paragraphe. C'est équivalent à l'utilisation des formats HTML (type `<p> <h1> <h2>` etc).
Voir aussi, le bouton d'annulation du style d'un texte, l'indentation, les symboles spéciaux.

Il est possible d'annuler ou de refaire la dernière action sur le texte avec les flèches undo et redo.

Pour ceux qui connaissent les HTML et le CSS (proposer l'animation du Pic correspondante), il est possible de personnaliser encore davantage l'article, en cliquant sur le bouton "Texte" à droite.
L'édition se déroule alors directement en mode HTML et CSS.

**Démontration** : Insérer dans la page `<h4 style="color:blue;">Les animations au Pic sont TOP !</h4>`. Sauvegarder la page et afficher sur la page publique.
	Revenir sur la page d'édition de l'article et présenter les différents boutons d'édition en mode HTML/CSS. Faire un exemple avec le bouton "b", qui va insérer autour d'un texte pré sélectionné dans l'article, le code <strong></strong> (ceci passe le texte en mode gras).

Sur la partie droite de l'article, nous trouvant une partie intitulée "révisions". Si on clique sur "parcourir", on peut voir en haut une barre des révisions. Si on faire défiler le curseur sur cette barre, on affiche les différentes versions de l'article. On peut ainsi très facilement gérer les modifications d'un article (gestion de configuration en quelques sortes).

On peut visualiser avec les couleurs rouges et vertes respectivement les suppressions et les ajouts.

Noter que l'on peur revenir à une version précédente à cliquant que le bouton "Rétablir cette révision".

**Démonstation** : Faire une démonstration de la gestion des version et restaurer une ancienne version d'un article.

Dans la partie supérieure du mode modification d'un article, il y a un bouton "Options de l'écran". Cette option permet d'afficher ou non certains éléments de l'écran dans la partie privée du site en mode modification d'un article.

**Démontration** : Enlever l'affichage des révisions en cliquant sur la case à cocher "Révisions". De le même manière il est possible d'ajouter les commentaires présents sur l'article.

**Remarque** : Pour améliorer la présentation de la page d'accueil du site, penser à ajouter la balise "lire la suite" dans tous les articles.

**Démontration** : Ajouter les balises dans tous les articles (la modification peut se faire dans plusieurs onglets du navigateur), puis retourner sur le site visible pour constater que la page d'accueil est maintenant bien plus lisible.

**Conclusion** : Nous avons vu comment utiliser l'éditeur de texte pour créer du contenu. Nous avons vu également comment personnaliser un article en utilisant le mode HTML/CSS. Si vous souhaitez consulter la référence de l'éditeur de contenu, vous pouvez consulter le site https://fr.wordpress.org/support/article/wordpress-editor/

## 6 - La gestion des images

Dans cette partie nous allons voir comment gérer les images dans WordPress. Notamment comme ajouter une image dans un article WordPress.

**Démonstration** :
	Dans un article existant, on se place dans le corps de l'article en mode édition. On clique alors sur le bouton "Ajouter un média".
	Il est alors possible d'ajouter un média soit directement en choisissant un fichier, ou soit en le choisissant depuis la bibliothèque de médias.
	Il est bien sûre possible d'ajouter au fure et à mesure chaque fichier. Il faut retenir que chaque fichier multimédia, va automatiquement s'ajouter dans la bibliothèque de médias.
	Voyons comment gérer la bibliothèque. On clique sur l'entrée de menu "Médias" dans le menu général de la partie privée de WordPress.
	On clique sur "Ajouter". WordPress nous propose de glisser les fichier (drag and drop) ou de les choisir via le gestionnaire de fichiers du système d'exploitation.
	Faire la démonstration en utilisant le Drag and Drop avec plusieurs fichiers.

	On retourne ensuite sur la modification des articles, puis on sélection l'onglet "Bibliothèque de médias". On insère alors une image mais via la bibliothèque. Noter à droite au moment de la sélection d'une image, les informations associées à l'image qui sera insérée : adresse web (image issues d'un site externe ou interne), Titre, Légende, Texte alternatif (accessibilité), Description. Ainsi que les réglages de l'affichage du fichier joint.

	Faire faire la manip. par les stagiaires.

**Remarque** : il est possible de modifier directement dans l'article, la mise en forme de l'image. Il suffit de sélectionner l'image à ajuster. Un menu contextuel apparait alors. Il est possible de changer l'alignement (droite, centre, gauche, pas d'alignement), ou de modifier (le crayon) les informations en lien avec l'image (taille, légende, etc.).

Notion d'image à la une (feature image). Il est possible d'utiliser une image pour illustrer (représenter) un article. On utilise pour cela sur la droite la zone "Image à la une". Il faut cliquer sur le lien "Mettre une image à une". La gestion de la suite est identique à l'intégration d'une image dans un article. On met à jour puis on bascule sur la partie visible pour voir les effets de la modification.
L'image est tout en haut de l'article, juste sous le titre.

Sur la page d'accueil du site, on voit également l'image qui permet d'illuster l'article associé.

**Démonstration** : Faire faire la manip aux stagiaires sur tous les articles du site école.
	Montrer le résultat sur la page d'acceuil principale du site après l'ajout des images à la une.

**Remarque** : il existe une fonctionnalité pour ajouter une gallerie dans le corps d'un article. Il suffit pour ceci de cliquer sur "Créer une galerie" au moment de l'ajout d'un média dans un article, puis de sélection plusieurs images dans la bibliothèque.

**Démonstration** : constater le résultat et surtout que le résultat n'est pas fameux avec les options par défaut.
	Remarque : il existe des plugin pour améliorer l'affichage des galleries dans les articles.

**Conclusion** : Nous avonc vu comment ajouter des images dans un article en utilisant éventuellement la bibliothèque de médias puis comment ajouter une image à la une.

## 7 - Intégrer des éléments externes (focus)

Comment intégrer des éléments externe dans un site WordPress ?

Quelques exemple :
* une vidéo de YouTube ;
* un post de FaceBook ;
* un tweet de Twitter ou Mastodon ;
* une carte OSM.

En gros des éléments qui appartiennent à d'autres plateformes qu'il est possible d'amener de ces différentes plateformes, et de les intégrer tels quels dans le site WordPress.

Au départ avec l'ancien éditeur il était nécessaire d'utiliser les iframe HTML mais depuis la version 5 un bloc embed est apparu, rendant la gestion des contenus embarqués bien plus simple à utiliser

	Exemple avec la bloc embed et youtube :
	https://youtu.be/ouvTudXsNtM
	Remarque : Pour les vidéos Youtube, il est possible de coller directement l'URL de la vidéo dans l'article en mode modification "Visuel", mais ce n'est pas recommander. Préférer à la place le mode <iframe> ou le bloc vidéo (plus simple à utiliser depuis la version 5).

	Démonstration avec le site OSM :
	Sélection la zone autour du bâtiment ou se passe l'animation, puis cliquer sur partager (à droite). Copier le lien HTML. Ce qui donne :
	<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=1.471948027610779%2C43.55104420729875%2C1.4735573530197144%2C43.553858995712595&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/#map=19/43.55245/1.47275">Afficher une carte plus grande</a></small>

	Basculer dans le mode d'affichage "Texte" en mode modification d'un article, puis insérer le code issu d'OSM.
	Visualiser sur le site visible, le résultat.

	Autre exemple avec Mastodon :
	<iframe src="https://chitter.xyz/@codl/99705723448836718/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://chitter.xyz/embed.js" async="async"></script>


Dans un iframe, il est possible de modifier la taille de la zone, exemple avec l'objet OSM précédent : width="425" height="350" ou en pourcentage. Par exemple 100% pour prendre toute la largeur du conteneur.

**Remarque** : on peut placer une vidéo à la place d'une image à la une. Il suffit de placer le block iframe, avant la balise "lire la suite" (c'est à dire dans l'extrait de l'article).

**Démonstration** :
	copier le iframe suivant dans la première partie d'un article en mode Texte :
	<iframe width="560" height="315" src="https://www.youtube.com/embed/z_qo20DiW1A?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Conclusion** : on a vue ici comment intégrer des éléments externes directement dans un article.

## 8 - Les Menus

L'objectif est de créer un menu destiné à faciliter la navigation dans le site.

On passe par le l'option du Menu : apparence > Menus du tableau de bord. On commence par lui donner un nom : par exemple menu principal, puis on cliques sur le bouton "créer le menu".
Les éléments à gauche permettent d'organiser le contenu. Ce sont ceux que l'on peut placer dans le menu.
Par exemple, un liens personnalisé permet de créer un renvoit vers la page d'accueil :
	- adresse web : http://blog1.jpegfr.net
	- texte du lien : ACCUEIL
Pensez à "Enregister le menu" avec le bouton du même nom.

Il est possible d'ajouter également des liens dans le menu, vers des catégories. Il suffit de cocher les cases correspondants aux catégories, que l'on veut placer dans le menu.

**Remarque** : on peut changer le nom du menu. Il faut cliquer sur un entrée du menu puis Titre de la navigation.

Pour afficher le menu dans la partie visible, il faut spécifier à quel endroit on souhaite placer le menu. Le choix est dépendant du thème qui est en place sur le site. Pour le thème par défaut, les options sont : Menu supérieur et Menu des liens de réseaux sociaux.
Pour notre exemple, on choisit "Menu principal", puis on enregistre le menu, avec le bouton du même nom.
On peut alors visualiser le résultat dans la partie visible du site.

**Démonstration** : Faire une démo des éléments en lien avec les menus.

**Remarque** : Les articles qui sont placés dans une sous catégorie sont visibles dans la page de la catégorie mère. Il est cependant possible de créer un élément de sous menu en décalant légèrement la nouvelle entrée de menu vers la droite.

**Remarque** : Le menu est présent sur toutes les pages du site web.

Il est possible de placer un article très populaire directement dans le menu (Sélectionner l'article dans le menu de droite de la page Menus du site privé). Il est possible aussi d'y placer une page. Nous verrons les pages plus loin.

**Démonstration** : On peut par exemple créer un second menu pour gérer les réseaux sociaux. Après configuration et sélection du bon endroit pour l'affichage de se nouveau menu (menu des liens de réseaux sociaux), le nouveau menu est visible en bas de page.

**Conclusion** : Dans cette partie, nous avons créé des menus, qui peuvent des entrées vers des pages, des articles, des catégories ou des liens (internes ou externes). On peut avoir des sous-menus. Nous allons maintenant voir comment utiliser les pages.

## 9 - Les pages

Nous allons aborder la notion de pages dans WordPress. On passe par l'option de menu "Pages" puis "Toutes les pages".

On peut commencer par supprimer la page par défaut. A ce propos, il est possible de placer la page dans la corbeille. Cela fonctionne comme avec le système d'exploitation, il est encore possible de récupérer l'élément dans la corbeille si besoin.

Nous allons ajouter deux pages : qui sommes-nous et contact (formulaire dans lequel les visiteurs pourrons nous envoyer un message via un formulaire).

On clique sur le bouton "ajouter".

**Démonstration** : On remplie la page puis le lien de menu.

Pour la seconde page de contact, nous allons utiliser un PlugIn. Nous verrons les plugins plus loin, mais pour le moment, nous avons besoin de créer une simple page vide.

Nous allons installer le plugin "Contact form 7". Passer par le menu "Extensions" puis "Ajouter". Recherche le plugin avec le nom spécifié.

**Remarque** : Lorsqu'on ne sait pas quel plugin utiliser il suffit de regarder sa popularité. On voit ici que celui ci est installé sur plus de 1 millions d'instances WordPress. Vérifier également s'il a été mis à jour récemment.

**Démontration** : Ajouter le plugin

	Si on clique à nouveau sur "Extensions", on voit que le nouveau plugin est maintenant présent dans la liste. Il faut cependant l'activer.
	Une fois activer, il est possible de le configurer en cliquant sur "Réglages".
	On voit également qu'un élément "Contact" est apparu sur le menu d'administration. Si on clique sur cet élément, on peut voir la liste des formulaires de contacts. Il en existe un par défaut.

**Démonstration** : Voir les paramètres du plugin.

	Noter la présence d'un short code : [contact-form-7 id="36" title="Formulaire de contact 1"]
	Il s'agit d'un sorte de méta langage du CMS WordPress. Ceci permet d'inclure le formulaire dans la page contact que nous avons créée tout à l'heure. Copions ce bout de code dans la page en mode visualisation, puis lancer un aperçu.
	On finalise en ajouter la page dans le menu du site.
	Noter qu'il est possible grace aux drag and drop de déplacer l'entrée de menu vers le haut par exemple, afin de réorganiser le menu.

**Conclusion** : Nous avons donc créé une page "Qui sommes-nous" et un page "Contact" que nous avons intégré au site via son menu principal. Nous avons eu un bref aperçu des plugin afin de réaliser le formulaire de contact. Dans la prochaine partie, nous allons voir à quoi servent les utilisateurs dans WordPress.

## 10 - La gestion des utilisateurs

Nous allons voir ici comment fonctionne la partie utilisateurs de WordPress.

Un bon exemple, lorsqu'on clique en haut à droite, on voit "picadmin" qui est le nom que j'ai choisi pour mon compte administrateur pour ce site école. Je peux donc modifier mon profil.

Pour accéder à la gestion des utilisateurs on peut passer par le menu "Utilisateurs" du Tableau de bord puis "Tous les utilisateurs".

**Démontration** :

	Cliquer sur l'utilisateur picadmin pour passer en revue les différents champs qui caractérises un utilisateur :
	- La case à cocher "Editeur visuel", permet de désactiver le mode d'édition WYSIWYG ;
	- Il est possible de changer le thème de couleur de l'interface d'administration ;
	- Il existe des raccourcies clavier permettant de modérer plus rapidement les commentaires associés aux articles. C'est interessant si le nombreux commentaires viennent enrichir le site ;
	- Il est possible de désactiver la barre d'outils en haut du site visible lorsque l'utilisateur est connecté ;
	- L'identifiant ne peut être changé, mais les coordonnées comme le prénom, le nom et un pseudo eux le peuvent ;
	- Le "modèle" à afficher (entre identifiant, pseudo, prénom et nom) ;
	- Le site web de l'utilisateur ;
	- Des renseignements bibliographiques (une présentation courte) ;
	- Une image de profil en utilisant le service gravatar (il s'agit d'une plateforme permettant d'associer une adresse courriel et une image d'avatar) ;
			Un avatar permet par exemple d'avoir un logo ou une image représentative du profil de l'utilisateur à coté des commentaires d'un article. Cela permet de l'identifier visuellement plus rapidement.
	- Le mot de passe peut être changer directement dans la partie gestion du compte ;
	- La connexion des sessions permet de fermer les sessions en cours sur toutes les machines où l'utilisateur serait connecté.

	Visualiser qu'après avoir modifier le mode d'affichage de l'identité de l'utilisateur, dans le site visible, le changement est visible. Par exemple le rédacteur d'un article et/ou la bio.

Si on revient maintenant dans le Tableau de bord, on constate qu'il est possible d'ajouter des utilisateurs. Un usage possible au sein d'une association est de permettre d'avoir une organisation éditoriale.
Il est possible ainsi de disposer au sein d'une association de plusieurs auteurs. Ce seront de nouvelles personnes, qui pourront se connecter sur le site WordPress.

**Démontration** :

	Demander aux stagiaires de se créer un compte de type "Auteur". Penser à bien définir le rôle "Auteur".
	Remarque : Un auteur peut rédiger, publier et gérer ses propres articles mais ne peut pas toucher à la forme du site. Noter que si les stagiaires se connectent au site, la partie Admin est différente que celle de l'Administrateur.
		- Un éditeur peut publier et gérer toutes les pages et articles de tous les utilisateurs (chef un rédacteur en chef) ;
		- Le contributeur peut rédiger et gérer ses articles, mais ne peut pas les publier (c'est un auteur un peu restreint pour les sites associatifs avec du contenu sensible qui a besoin d'être validé avant publication) ;
		- L'abonné n'a uniquement accès à son profil, qu'il peut gérer et il peut commenter des articles.

**Remarque** : il est possible de s'enregistrer sur un site WordPress sans passer par l'administrateur, si le site est configuré en conséquence. C'est dans "Réglages" > "Généraux" > "Inscription" : cocher la case "Tout le monde peut s'enregistrer". C'est intéressant par exemple pour les abonnés. Le role par défaut des nouveaux utilisateurs est justement configuré sur "Abonné".

**Démonstration** : Cocher la case en question et faire constater le changement dans le formulaire de login de WordPress.

**Conclusion** : nous avons vu rapidement la gestion des utilisateurs. Nous allons voir maintenant comment il est possible avec WordPress de gérer les commentaires.

## 11 - Les commentaires

Cette partie va traiter des commentaire. Nous avons constaté qu'à la fin de chaque article, il est possible de laisser un commentaire. Lorsque un utilisateur est connecté, c'est avec ses identifiants (et ses informations associées), il va proposer un commentaire. Dans le cas d'un utilisateur non authentifié, le visiteur devra laisser des informations complémentaires comme son adresse de courriel par exemple.

**Démonstration** :

	Faire la démonstration depuis une fenêtre d'un autre navigateur dans lequel l'utilisateur n'est pas authentifié.
	Faire une saisie d'un commentaire. Noter que dans le navigateur du visiteur, le commentaire s'affiche mais avec le status 'en attente de modération'.
	Si on va maintenant dans le navigateur de l'administrateur, dans la page du site visible, on ne voit pas le commentaire. Il faut donc le modérer.
	Pour faire cela on se dirige dans la partie Tableau de bord du site. On voit apparaitre le chiffre "1" à proximité du menu "Commentaires". Il s'agit d'une notification de commentaire dans la partie visible du site.
	Il est possible à de stade de l'approuver pour qu'il s'affiche sur le site publique, y répondre directement, le rendre indésirable ou le placer dans la corbeille.
	On va y répondre directement.
	Retourner sur le site visible pour voir le résultat.

La configuration de la modération est accessible via le Tableau de bord, en passant par le menu "Réglages" > "Discussion".
Les trois premières options sont en lien avec la mise en place d'un commentaire sur un autre site. Si l'article est très souvent cité sur d'autre site, les pings de notifications peuvent être nombreux. Il est alors possible dans ces options de les supprimer.

**Démonstration** :

	Parcourir les différentes options.
	Tester l'usage des gravatars générés automatiquement en basculant sur le site visible.

**Remarque** : Il est également possible d'appliquer un traitement particulier des commentaires à un article en passant directement par l'interface d'édition de l'article en question.

**Conclusion** : Nous avons vu comment WordPress pouvait nous faciliter la tâche pour gérer les commentaires des articles, ce qui permet de gérer l'activité autour du site.

## Conclusion

Nous voilà à la fin de cette animation dédiée à la prise en main de WordPress. Nous espérons vous avoir donné envie d'aller plus loin !

Le Pic.org propose une animation dédiée à la personnalisation d'un site sous WordPress. Nous vous invitons à consulter notre agenda afin de connaitre la prochaine session ...

Ce dépôt sera mis à jour régulièrement avec les notes des nouvelles animations en lien avec le CMS WordPress et sera régulièrement actualisé :
[Liste des animations](../master/README.md)
