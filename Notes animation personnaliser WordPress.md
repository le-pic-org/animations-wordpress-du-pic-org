# Notes animation Personnaliser son site sous WordPress

## Plan présentation

1. Intégrer des Widgets
2. Ajouter un thème à son site
3. Personnaliser un thème
4. Ajouter des plugins
5. Le FSE : Est ce la fin des Pages Builder ?
6. Créer un Header/Footer avec le FSE
7. Les Patterns Gutenberg et la palette globale

Vous savez comment écrire des pages et des articles dans un site wordpress ? Peut-être avez-vous suivi notre atelier "WordPress : Prise en main"

Vous voudrez aller plus loin dans la maîtrise de votre site : comment créer des menus ? Comment mettre des widgets sur vos pages ? D’ailleurs c’est quoi un widget ? L'arrivée du FSE (Full Site Editing) dans la dernière version de WordPress vous inquiète ?

Dans cette animation, nous allons vous guider à travers les options de personnalisation du thème WordPress, vous aider à choisir celui qui vous convient et vous montrer comment le faire de manière sûre et efficace. Nous verrons également comment ajouter de nouvelles fonctionnalités au site grâce aux Widgets et aux plugins mais aussi comment modifier et personnaliser l’ensemble de l’interface d’un site à partir de blocs de contenus éditoriaux grâce aux dernières fonctionnalités de Gutenberg. 

**Objectif** : Savoir personnaliser un site élaboré avec WordPress.

**Pré-requis** : Avoir déjà travaillé avec WordPress, ou avoir suivi les ateliers sur Wordpress.

**Matériel** : Vous pouvez apporter votre ordinateur, portable ou non, équipé d'une carte réseau filaire ou wifi, et en état de marche. Nous utiliserons uniquement le navigateur internet. Veillez à ce qu'il soit à jour.
Si vous ne pouvez pas venir avec un ordinateur portable, précisez-le nous afin que nous puissions vous en prêter un (dans la mesure de nos disponibilités).

**Logiciels nécessaires** : votre navigateur préféré, il faut juste qu'il soit à jour

**Présentation PDF** : [Présentation personnaliser son site WordPress](../blob/master/Présentations/PrésentationWPPersonnalisation.pdf)

## Avant propos

La personnalisation d'un site WordPress via l'ajout de directives CSS n'est possible que sur un site construit à partir de WordPress.org. La plateforme wordpress.com ne permet la personnalisation via les CSS que sur la version prenium, payante.

Depuis la version 5.6 l'éditeur en ligne Gutenberg qui utilise les blocs éditoriaux est présent de plus en plus dans toutes les phases de création du site. Avec la dernière version il devient même possible de mettre en forme le site complet y compris le design, ce qui n'était possible jusque là qu'avec les constructeurs de page comme Elementor.

Nous allons parcourir les fonctionnalités de personnalisation traditionnelles disponibles avec WordPress puis nous verrons les atous qu'apporte le FSE.

## 1 - Intégrer des Widgets

Chaque Widget est une sorte de bloc sur la partie visible d'un site. Ils agissent comme des "conteneurs de contenus indépendants". Ils permettent de mettre en avant des informations spécifiques et/ou des options à destination des visiteurs du site.

Exemples :

* un bloc de recherche ;
* articles récents ;
* commentaires récents ;
* archives ;
* catégories ;
* méta.

La gestion des widgets est accessible à travers le Tableau de Bord, en cliquant sur le menu "Apparence" > "Widgets".

**Petit rappel** : comme nous l'avons vu pour les menus, l'emplacement de certains éléments va dépendre du thème WordPress, que l'on a choisit de mettre en place. Pour les widgets, c'est la même chose.

**Remarque** : dans le dernier thème actuel (Twenty Twenty one) dela branche 5.6, il n'y a plus de barre latérale. Mais selon le thème choisi, on sera possible de la retrouver.
Dans ce thème, il n'est possible d'ajouter un Widget que dans le Pied de page.

Par exemple, pour le thème de la version 4.9, on pouvait constater trois emplacements disponibles sur la page web visible :

* Barre latérale ;
* Contenu du bas 1 ;
* Contenu du bas 2.

Les widgets disponibles sont ceux qu'il est possible de mettre en place dans l'un des 3 emplacements dont nous venons de parler.

**Remarque** :
 Par exemple le widget "Texte", permet de placer une zone de texte arbitraire mais aussi du contenu HTML. C'est très intéressant pour mettre en place des `<iframe>` comme par exemple une vidéo Youtube.
 A noter que pour ce cas précis, il suffit de copier simplement d'URL Youtube dans le champ texte ou utiliser le widget "Vidéo".
 A noter également le widget "Menu de navigation" qui permet par exemple d'ajouter un menu latéral avec par exemple un lien vers une page "A propos".

**Démontration** :

 Mettre en place le widget "Articles récents" dans le "Pied de Page" puis le widget "Catégories", puis les widgets "Texte", "Menu de navigation" avec un menu dédié.
 Constater que chaque widget ainsi mis en place, est accompagné de plusieurs paramètres de configuration.
 Voir les résultats sur le site visible.
 Ajouter la place "Nuage d'étiquettes" et "Calendrier".

**Conclusion** :
 Il existe un certain nombre de wigdets disponibles par défaut dans WordPress. La liste des widgets disponibles dépends également du thème installé mais aussi des plugins installés.
 Ces multiples possibilités permettent de personnaliser un site WordPress de manière très modulaire.

## 2 - Ajouter un thème à son site

La gestion des thèmes passe par le menu "Apparence" > "Thèmes" du Tableau de bord.

Nous pouvons apercevoir 3 thèmes par défaut dans cette interface. Ils s'agit des thèmes officiels installés par défaut avec WordPress. Le nom de chaque thème correspond à l'année de sortie du thème (2019, 2020, 2021).

Chaque thème correspond à un ensemble de fichiers présents sur le serveur web.

**Noter** : Faire bien la différence entre un thème installé et un thème activé.

**Démonstration** :

 activer un autre thème et l'afficher dans la partie visible.
 Aller voir dans la configuration des widgets pour constater que les widgets et les emplacements ont évolué selon le thème.
 Idem pour les menus (les emplacements sont différents).

**Remarque** : Tous les thèmes WordPress sont Responsive

**Démontration** :

 Réduire la taille des fenêtres pour voir les effets. Montrer les fonctions d'analyse responsives des outils développeurs de Firefox.

**Principe important** : Les thèmes ne changent pas le contenu, mais le look du site.

Dans la fenêtre "Ajouter des thèmes", nous nous trouvons dans une grande bibliothèque permettant de sélectionner le thème gratuit que nous préférons.
Chaque vignette correspond à un apercu de ce qu'il est possible de faire avec le thème correspondant. Le thème défini le cadre mais c'est le contenu qui fait un site web.

Il y a des fonctions de filtrage des thèmes.

**Démontration** :

* Visualiser les thèmes ;
* Filtrer les thèmes selon quelques critères.

**Remarque** :
 Plus un thème est populaire, plus il est maintenu et facile à mettre en oeuvre.
 Il est conseillé de naviguer pour trouver son thème, qui dépend du type de contenu. Il faut faire des essais...

**Démonstration** :

 Ajouter le thème Sparkling
 Constater dans la page Widget, que de nouveaux widget sont disponibles ainsi que de nouveaux emplacements de Widgets.

Il est possible de mettre en place des thèmes originaux ou payants (premium) en passant par un site externe.

Il est ainsi possible de télécharger des thèmes et de les installer manuellement (via FTP/SFTP en utilisant un logiciel de copie de fichiers comme FileZilla,
dans le dossier WordPress-content/themes dans un répertoire spécifique du nom du thème), ou via le Tableau de bord.

**Démonstration** :

 Montrer avec FileZilla comment se connecter au site à distance.

Le site qui fait référence pour récupérer des thèmes est : themeforest.net (<https://themeforest.net/category/WordPress>)

On trouve aussi des templates HTML.

Même si les tarifs ne sont pas excessifs tous les thèmes sont payants.

Il existe tout un système de filtres qui permet de cibler un peu plus notre besoin :

* tags ;
* tarif ;
* nombre de ventes (si le thème n'a pas été vendu souvent, ce n'est pas bon signe) ;
* la note (rating) ;
* le mode de compatibilité avec certains plugin.

Il est possible de classer les thèmes :

* par tarif ;
* par nombre de ventes ;
* etc.

Consulter aussi le support du thème pour vérifier s'il est actif.

**Démonstration** :

 Naviguer dans la page et explorer l'un des thèmes.

**Note** : voir le rating et surtout les avis (reviews).

Les commentaires sont les demandes d'assistance sur le thème.

Dans les détails du thème, le bouton "Live Preview", permet d'afficher un apercu. Il peut parfois exister plusieurs exemples.

Il est même parfois possible d'installer directement la démo, ce qui permet de commencer le site avec une base complète. Seul le contenu sera à ajouter.

La zone de recherche de thèmes, permet de pré-filtrer (par exemple "blog photo").

Une recherche sur un moteur de recherche permet de trouver des petites pépites. Par exemple : Twentig qui permet de customiser le thème WordPress par défaut Twenty Twenty.

**Conclusion** : Nous venons de voir comment ajouter un thème dans son site WorPress. Dans la prochaine partie nous allons voir comment personnaliser ce thème.

## 3 - Personnaliser un thème

La personnalisation est accessible depuis la barre supérieure du site si l'on est connecté ou via le Tableau de bord : il s'agit l'outil de personnalisation WYSIWYG (Apparence > Personnaliser).
Il est possible de personnaliser la présentation du thème activé via le bouton "Personnaliser", ce qui est un accès équivalent à l'outil de personnalisation.

**Démonstration** :

Personnaliser un thème (Faire directement la personnalisation en ligne) :

* Il est possible d'ajouter une icone au site web. Il s'agit de l'option "Identité du site" ;
* de modifier les couleurs ;
* de placer une images d'en-tête ;
* une image d'arrière plan ;
* Les menus (c'est identique à ce que l'on fait depuis l'interface du Tableau de bord) ;
* Les widgets (idem) ;
* Sélection le type de la page d'accueil (on peut par exemple utiler une page spécifique plûtot que la liste des articles) ;
* Il est possible d'ajouter des nouveaux thèmes. Il faut cliquer sur ajouter un thème.

**Remarque** :
Le menu social n'est plus visible après avoir mis en place le théme Sparkling. Retourner dans la personnalisation des thèmes pour le réactiver.
Il existe aussi un widget starling réseau sociaux. Il faut l'ajouter pour visualiser les icones.
Pour sortir du mode de Personnalisation du thème il suffit de cliquer sur la croix en haut à gauche.

## 4 - Ajout des plugins

Un plugin est une extension en français.

C'est l'avantage de WordPress : la modularité.

Par exemple, pour partager un article automatiquement sur Facebook, il faut ajouter un plugin. Autre fonction envoyer une newsletter, ou optimiser le SEO (référencement), améliorer la rapidité (cache),
transformer le site en site de eCommerce.

La gestion de plugin se passe dans le menu "Extensions" du tableau de bord. Par défaut dans la branche 5.6, certains plugin sont pré installés : Akismet par exemple.

Si on clique sur "Ajouter", on accède à la bibliothèque des thèmes. C'est très similaire à la gestion des thèmes.

Chercher les plugin avec des termes en anglais (peu de résultats en français). Exemple : "slider" (Slide Anything ...).

Constater les informations de la dernière mise à jour entre les deux premiers résultats, le nombre de raking (notes) et le nombre d'installations actives.

Il est possible d'installer le plugin via le SFTP. Le répertoire d'installation sera dans ce cas : WordPress-content/plugins. C'est cependant plus simple depuis le tableau de bord à condition
d'avoir bien configuré le plugin sftp updater installé par défaut sur les instances WordPress qui sont installées par le Pic.

**Démonstration** :

Configuration du plugin SSH SFTP Updater Support.

Après avoir installé le plugin, il faut aussi l'activer. Il est bien sure possible d'activer plusieurs plugin en parallèle.

On verra l'ajout de plugin par la suite.

**Remarque** : Nous proposons certains Plugin par défaut :

* SSH SFTP Updater Support : permet la mise à jour à l'ajout des thèmes, plugin, etc en tenant compte des spécificités de l'hébergement du Pic ;
* Contact Form 7 : pour créer des formulaires de contact par exemple ;
* TablePress : pour insérer des tableaux de données issus d'un tableur.

## 5 - Le FSE : Est ce la fin des Pages Builder ?

Quelques Builder de site :
Divi, Elementor, Beaver Builder, Visual Builder, Oxygen

A long termes, il est probable que le FSE de Gutenberg risque de rattraper les autres constructeurs de site mais en dehors des thèmes gratuits proposés par WordPress, l'éditeur de site Gutenberg n'est pas encore majoritaire dans les usages.
TODO: à intégrer dans l'animation personnalisation du thème WP.

* Kadence block ;
* Gutenberg Blocks - Ultimate addons.

Avantages de travailler avec Gutenberg :

* La vitesse, qui permet d'améliorer le SEO (Google prends en compte la vitesse de chargement dans son algorithme de référencement), le code est plus léger car il nécessite moins de plugin à résultat visuel identique (jusqu'à près de 3xmoins de code) ;
cf <https://wptavern.com/gutenbergs-faster-performance-is-eroding-page-builders-dominance>
* Davantage en ordre avec les standards du Web (moins de code redondant, architecture plus propre 356 Divs pour Elementor contre 77 pour Gutenberg sur une page de test donnée) ;
cf <https://gutenberghub.com/gutenberg-vs-elementor-html-bloat/>

**Démonstration**
Ouvrir le site école, activer un thème compatible avec le FSE (Twenty Twenty-Two ou Three) et montrer que pas mal de choses ont disparu dans le menu 'Apparence'. On ne trouve que éditeur.
cf images Menu Apparence_Thème ...

Accès à l'éditeur via l'interface d'admin (Apparence > Editeur) ou depuis le site en cliquant sur "Editer le site" en haut dans la barre de fonction après connexion au site WP.

Premier élément important pour modifier le site : la vue list View. On clique sur les trois trais verticaux (cf image ListView). C'est identique à ce que l'on trouver dans Divi ou Elementor.

Montrer que lorsqu'on navigue sur le List View à gauche, on sélectionne l'élément à personnaliser à droite.

Rappel du fonctionnement de Gutemberg (vu dans l'animation prise en main) :

Cliquer sur le titre de l'article puis Cliquer sur le menu de personnalisation (cf image FSE_Personnaliser). La vue de personnalisation à droite s'affiche alors (cf image FSE_Personnaliser_2). Si on change de bloc la vue de gauche permet de voir de quel type de bloc il s'agit (faire la démo).

C'est donc dans cette vue (à droite) que l'on va pouvoir faire toutes les modifications sur les blocs. Certains plugins permettent d'ajouter des blocks (cf intro) :

* Kadence block ;
* Gutenberg Blocks - Ultimate addons.

Le premier est le plus interessant. TODO : Intégrer une démo de Kadence dans l'animation Personnalisation du thème WP. (cf <https://www.youtube.com/watch?v=SYb24ncjVsY>)

Montrer comment ajouter un block dans la page (par exemple un paragraphe ou une gallerie sous le titre de l'article - faire la démo avec les deux types de blocs). Montrer comment ajouter des colonnes (par exemples ajouter trois colonnes)

Montrer que dans chaque colonne on peut ajouter des éléments.

Jusqu'ici rien de nouveau par rapport à la gestion des blocks déjà existante avec Gutemberg.

## 6 - Créer un Header/Footer

Les nouveautés sont accessibles en cliquant sur le logo WordPress (cf image FSE_Personnaliser_3 et 4).

On voit Modèles et Elèments de modèles :

* Modèles permet de modifier une page spécifique (Accueil, Index, Page, Recherche, etc...) ;
* Eléments de modèles permet de modifier une partie d'un modèle (une partie de la page comme le Footer, Header, etc).

Si on clique par exemple sur Eléments de modèles > Header (Dark, small), on va modifier uniquement le Header du site.

Si on clique sur Modèles puis Accueil on accède à la modification de la page d'accueil du site. Noter que on peut modifier le Header directement dans la page d'acceuil.

Montrer comment modifier le Header en changeant la couleur de fond par exemple. Tester avec les dégradés. Changer l'angle, le style.

Montrer comment supprimer l'image de la page, modifier le logo du site (utiliser le logo du Pic), modifier sa taille (à droite dans les options du block).

Le titre du site est un élément ajouter que l'on peut remplacer facilement également par un autre type de block (titre ou paragraphe par exemple).

Montrer qu'il est possible de revenir en arrière avec Control Z ou les flêches de retour en arrière.

Il existe un menu par défaut dans le thème Twenty Twenty-Two 'Sample Page'. Laissons çà ainsi et créeons un nouveau menu. La création des menus passe maintenant par le block 'navigation'.
cf <https://make.wordpress.org/core/2022/01/07/the-new-navigation-block/>
<https://www.youtube.com/watch?v=ZFl_6I5_AZk&t=40s>

On peut aussi jouer avec le padding de la zone d'entète pour modifier le style.

Remarquer qu'au moment de la sauvegarde, WP propose les éléments qui vont être impactés. On valide.

Gestion des parties de modèles personnalisés. Il faut cliquer sur Elements de modèles puis ajouter un élément de modèle en haut à droite. Sélectionner la zone concernée par l'élément de modèle (par exemple En-tête ou Header). Puis 'Créer'.

Remarquer que si on retourne sur la liste des élements de modèles, le nouveau modèle est disponible est appartient à l'utilisateur qui vient de le créer. Le point bleu à droite montre que le modèle a été modifié par rapport à la version de base du thème.

Exercice modifier le footer en ajoutant en partie centrale un autre menu, supprimant la référence vers WP.

## 7 -  Patterns Gutenberg et la palette Globale

Les patterns sont disponibles dans la partie de droite à coté des blocs. Il s'agit de compositions pré définis permettant de gagner du temps pour ajouter un design spécifique.

Tester quelques pattern.

Quelques thème compatibles FSE : Makoney, Blockbase, Ona. Mais aussi Aino, Frost, Tove, Pria

Pour sélectionner un thème, aller dans Apparencer > ajouter un nouveau Thème

On peut cliquer sur Filtre de Fonctionnalités pour affiner la recherche (par exemple Editeur de site pour sélectionner les Thèmes FSE).

Les styles permet de changer l'affectation d'une police selon les éléments. Le nombre de police disponible dépend du Thème. Il est aussi possible de modifier pour chaque élément une police ou des couleurs par défaut.

Montrer l'intérêt de la Query loop.

## Conclusion

Nous voilà à la fin de cette animation dédiée à la personnalisation d'un site sous WordPress. Nous espérons vous avoir donné envie d'expérimenter par vous-même !

Le Pic.org va surement proposer dans l'avenir de nouvelles animations. Nous vous invitons à consulter notre agenda afin de connaitre les prochaines session ...

Ce dépôt sera mis à jour régulièrement avec les notes des nouvelles animations en lien avec le CMS WordPress et sera régulièrement actualisé : [Liste des animations](../master/README.md)
