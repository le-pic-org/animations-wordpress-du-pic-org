# Évolutions de WordPress par version depuis novembre 2024

## **WordPress 6.7 (Novembre 2024)**
- **Nouveau thème par défaut :** Introduction de *Twenty Twenty-Five* avec des designs modernes et personnalisables.
- **Aperçu global :** Nouvelle fonctionnalité *Zoom Out* pour une vue d'ensemble du site, facilitant la navigation et l'organisation.
- **Champs personnalisés pour les blocs :** Possibilité d'ajouter des champs personnalisés directement aux blocs pour plus de flexibilité.
- **Préréglages de taille de police :** Ajout de préréglages pour assurer une cohérence typographique sur le site.
- **Support des images HEIC :** Compatibilité avec le format HEIC utilisé sur les appareils Apple.
- **Améliorations des performances et de l'accessibilité :** Optimisations continues pour une meilleure expérience utilisateur.

**Source :** [VersionLog - WordPress](https://versionlog.com/wordpress/)

---

## **WordPress 6.6 (Juillet 2024)**
- **Palette de couleurs et choix de polices supplémentaires :** Extension des options de personnalisation pour les concepteurs.
- **Aperçus rapides des pages :** Prévisualisation rapide des pages pour une édition plus efficace.
- **Restauration des mises à jour automatiques des plugins :** Option de revenir à une version précédente après une mise à jour problématique.
- **Surcharges de styles de blocs :** Personnalisation plus granulaire des styles de blocs.
- **Améliorations des performances et de l'accessibilité :** Optimisations continues.

**Source :** [VersionLog - WordPress](https://versionlog.com/wordpress/)

---

## **WordPress 6.5 (Avril 2024)**
- **Gestion des polices Google :** Intégration facilitée via une bibliothèque de polices.
- **Historique des révisions :** Suivi des modifications des styles dans le *livre de styles*.
- **Outils de mise en page :** Gestion avancée des arrière-plans, ratios d'aspect et ombres portées pour les blocs.
- **Vues de données et glisser-déposer améliorés :** Interface plus intuitive pour manipuler blocs et données.
- **Nouvelles API :** Améliorations pour les développeurs avec des fonctionnalités d'interactivité et de liaison de blocs.
- **Outils d’apparence pour les thèmes classiques :** Personnalisation des thèmes sans fichier `theme.json`.
- **Dépendances de plugins :** Gestion des dépendances pour assurer la compatibilité.
- **Améliorations des performances et de l'accessibilité :** Optimisations générales.

**Source :** [VersionLog - WordPress](https://versionlog.com/wordpress/)

---

## **Dernière version en cours**
La dernière version stable de WordPress est **6.7**, publiée en novembre 2024. Elle introduit des améliorations majeures comme le thème *Twenty Twenty-Five*, des outils de personnalisation étendus, et des optimisations en termes de performance et d’accessibilité.

---

### **Sources utilisées :**
- [VersionLog - WordPress](https://versionlog.com/wordpress/)
- [Archive des versions – WordPress.org Français](https://fr.wordpress.org/download/releases/)
- [Versions de WordPress – Documentation](https://wordpress.org/documentation/article/wordpress-versions/)

---

