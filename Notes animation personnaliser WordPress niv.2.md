# Personnaliser le thème visuel de son site sous WordPress

Aprés avoir vu comment ajouter des fonctionnalités à son site sous WordPress, comment choisir un thème et l'adapter avec les outils installés par défaut avec WordPress, nous allons voir comment modifier plus en profondeur un thème ou en créer un complétement original.

Cette animation se déroulera en deux parties :

1. Personnalisation d'un site WordPress avec le constructeur de site Elementor ;
2. Exemple de création d'un thème personnalisé avec HTML, CSS et PHP.

**Objectif** : Pour ceux qui connaissent WordPress et qui veulent complétement modifier la présentation de la partie visible de leur site. L'idéal étant d'avoir suivi les animations [Rédiger son site Web avec WordPress](https://www.le-pic.org/spip.php?article806), [Personnaliser son site WordPress](https://www.le-pic.org/spip.php?article631) et [Personnaliser son site Web avec les CSS](https://www.le-pic.org/spip.php?article631), cependant une connaissance des fonctionnalités principales de WordPress et de HTML est suffisante.

**Pré-requis** : un minimum de connaissances de WordPress du HTML, de PHP et de CSS

**Matériel** : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.

**Logiciels nécessaires** - Si vous apportez votre machine, merci de les installer avant le début de la session pour réserver un maximum de temps au monitorat :

 * Le navigateur Firefox ou Chromium ;
 * Un éditeur de texte (mais pas un traitement de texte) : par exemple Notepad++ sous Windows, ou Geany sous Gnu/Linux.

# Partie 1

Dans allons voir dans cette première partie l'utilisation de base du constructeur de pages WordPress Elementor.

Elementor permet de créer des pages esthétiques de manière très simple.

CF. https://elementor.com/

On peut insérer des composants, des modèles d'images et de Templates (modèles de page) que l'on peut par la suite personnaliser.

Elementor est un plugin Freemium. Il dispose donc de fonctionnalités gratuites (essentielles) et des fonctionnalités payantes dans la version Elementor Pro.
Les fonctionnalités de base sont déjà suffisantes.

Les tarifs de la version Pro sont :
* 49$ pour un site ;
* 99$ pour 3 sites ;
* 199$ pour un nombre illimité de sites.
## Installation de Elementor

Via l'interface d'administration de WordPress on cliques sur Extensions ▸ Ajouter, puis on cherche dans la liste des plugin en utilisant le moteur de recherche : Elementor.

![Installation d'Elementor](./NotesIMG/InstallationElementor.png "Installation d'Elementor")

Puis on cliques sur installer maintenant. Puis Activer lorsque le bouton change de couleur.

Une fois l'installation achevée, on obtient une fenetre de personnalisation.

![Installation d'Elementor - Etape 2](./NotesIMG/InstallationElementor2.png "Installation d'Elementor  - Etape 2")

On peut décocher "Become a super contributor by sharing non-sensitive data." et cliquer sur "Passer".

On passe alors à l'étape 2 de l'assistant d'installation, qui nous propose d'installer un thème dénommé "Hello". 

Hello est un thème vide prévu pour créer une présentation à l'aide d'Elementor. Il est tout à faire possible d'installer ce thème par la suite ou d'utiliser Elementor avec n'importe quel autre thème.

![Installation d'Elementor - Etape 3](./NotesIMG/InstallationElementor3.png "Installation d'Elementor  - Etape 3")

On clique sur le bouton "continue with Hello theme".

On peut alors préciser le nom du site ou passer :

![Installation d'Elementor - Etape 4](./NotesIMG/InstallationElementor4.png "Installation d'Elementor  - Etape 4")

On préfère valider le nom du site. On clique sur Suivant.

On peut alors choisir le logo de son site. Soit en utilisant une image de la librairie ou en ajoutant une nouvelle image.

![Installation d'Elementor - Etape 5](./NotesIMG/InstallationElementor5.png "Installation d'Elementor  - Etape 5")

On choisi ce qu'on veut !

On peut finaliser l'installation en choisissant le type de Template par défaut. Soit un Template vide ou alors sélectionner un Template parmis la collection disponible sur Internet ou un Template que l'on aurait déjà mis ou point soit-même.

Dans un premier temps, commencons avec un modèle vide.

![Installation d'Elementor - Etape 6](./NotesIMG/InstallationElementor6.png "Installation d'Elementor  - Etape 6")

L'installation est achevée, on arrive sur le tableau de bord d'Elementor.

![Installation d'Elementor - Etape 7](./NotesIMG/InstallationElementor7.png "Installation d'Elementor  - Etape 7")

**Remarque** : on peut basculer sur l'éditeur par défaut de WordPress en cliquant sur le menu en haut à droite puis "Revenir à l'Editeur WorPress".

![Installation d'Elementor - Etape 8](./NotesIMG/InstallationElementor8.png "Installation d'Elementor  - Etape 8")

## 2 - Créer une page avec Elementor

On bascule sur l'éditeur WordPress traditionnel afin de voir comment modifier une page existante de son site avec le constructeur de pages.

On choisi une page puis on clique sur "Modifier avec Elementor".

L'éditeur s'ouvre (voir étape 7).

## 3 - Modèles et sections

A l'ouverture d'une page vide, on trouve deux boutons : ajouter une section et sélectionner un modèle.

Si l'on clique sur sélectionner un modèle on va pouvoir choisir parmis une grande quantité de modèles créés par Elementor.

Il y a des modèles gratuits et des modèles qui ne sont disponibles qu'avec la version Pro (marqueur Pro ou expert en haut à droite).

En cliquant sur un modèle on peu voir un visuel du modèle.

Il est également possible de récupérer un modèle existant par ailleurs hélas on trouve pas mal de ressources payantes ou qui nécessite la version Pro.

On va commencer par récupérer un modèle gratuit de la bibliothèque. Une fois un modèle retenu on clique sur Insertion.

Hélas, le processus d'installation nécessite de créer un compte gratuit chez Elementor.

Après l'inscription et la synchronisation du modèle avec l'instance de WordPress, le modèle est éditable.

Penser à cliquer sur "Mettre à jour" après chaque modification importante.

Il est possible de prévisualiser le résultat en cliquant sur l'icone en forme d'oeil à coté de "Mettre à jour".

On va commencer par ajouter une nouvelle sections entre deux sections existantes. Il suffit de cliquer sur le "+" de la section inférieure.

![Ajouter une section - Etape 1](./NotesIMG/AjouterSection.png "Ajouter une section - Etape 1")

![Ajouter une section - Etape 2](./NotesIMG/AjouterSection2.png "Ajouter une section - Etape 2")

**Remarque** : Il est tout à fait possible d'ajouter un modèle dans cette section et donc de lier la section à un nouveau modèle.

On va sélectionner "Ajouter une nouvelle section". On va sélectionner le type de section (pleine, colonne, etc). Choisissons pas exemple deux colonnes.

Lorsqu'on clique sur les 6 petits points en haut de la section on va pouvoir modifier les paramètres de cette section sur la gauche.

**Remarque** : Elementor dispose maintenant d'un navigateur (à gaucge) de section et de widget qui permet très facilement de naviguer au sein de son modèle.

Explorons les paramètres :
* Etirer la section : permet d'utiliser toute la largeur de la page (assez courant) ;
* Largeur du contenu : option qui permet d'encadrer du contenu à l'intérieur de la section ;
* Si on a sélectionné "Encadrer", on peut jouer alors avec la largeur ;

On peut également modifier l'écart des colonnes, la hauteur ainsi que la alignement vertical (haut, milieu, bas etc).

Outre la mise en page, on peut aussi appliquer un style (icone au milieu) :

![Modifier une section - les onglets](./NotesIMG/ModifierSection.png "Modifier une section - les onglets")

Cliquons pour commencer sur "Arrière plan" puis classique. On peut par alors modifier l'image de fond.

Dans "avancé" on va pouvoir modifier les marges de la section.

### Gestion du contenu

A l'intérieur des sections, après avoir inséré des colonnes, on va pouvoir ajouter du contenu. Ce contenu peut etre des images du texte, etc ...

Par exemple on va insérer un ensemble "Box icone", il s'agit d'un texte qui dispose d'une icone. Pour faire cela on fait glisser dans le contenu de la colonne un widget "Section interne" pour scinder en deux section la colonne courante. Puis dans la colonne de gauche on fait glisser une icone. Dans la nouvelle colonne de droite un widget "Editeur de texte". 

**Remarque** : On jouant avec la bordure d'une colonne on peut modifier les largeurs relatives.

Modifier par exemple la couleur de la police, et dans "avancé" les effets en ajoutant par exemple un effet "Zoom in down".

**Remarque** : il y a un menu contextuel lorsque on fait un clic droit sur l'icone en haut à gauche d'une section/colonne. On peut par exemple dupliquer la colonne ou la supprimer via ce menu.

**Remarque** : Au sujet du menu "Balise HTML", il s'agit d'une option permettant d'améliorer le référencement naturel en spécifiant un type spécifique de balise plutôt que la valeur par défaut appliqué par Elementor, à savoir un div. Si on regarde le changement entre deux paramètres à travers l'inspecteur (F12 et ctrl+shift+C) on observe bien le changement.

On peut modifier la distance entre les widgets dans la colonne avec l'option "Espace des widgets" après avoir sélectionné la colonne.
On peut modifier la couleur de la colonne ou une bordure sans que cela affecte la présentation de la section. On peut aussi ajouter une image uniquement dans la colonne.

Présentation de quelques widgets ...

On peut ajouter de nouveaux widget. Rechercher sur internet "Widget Elementor". Il s'agit en général de plugin wordpress, qui s'installent donc classiquement.

### Conception réactive

Elementor permet d'éditer directement la conception de son modèle selon le type de terminal sur lequel sera affiché le site.

Il suffit de cliquer sur le bouton "Mode responsive" en bas à gauche.

On peut alors éditer pour le mode sélectionné (téléphone par exemple) chaque widget, section, colonne spécifiquement.

On peut vérifier que l'on modifie bien le mode d'affichage spécifique pour téléphone lorsque qu'une icone de téléphone est affichée à coté du paramètre de présentation à adapter.

On peut faire disparaître un élément en via "Avancé" puis "Responsive" et marquer le mode que l'on souhaite faire disparaître.
### Enregistrement des modèles

Il est possible d'enregistrer une partie du modèle (section, colonne, etc ...) en utilisant l'option "Enregistrer comme modèle" :

![Enregistrement d'un modèle](./NotesIMG/EregistrementModèle.png "Enregistrement d'un modèle")

### Conclusion

N'hésitez pas à tester et laissez parler votre créativité.
# Partie 2

## 1 - Personnaliser certains éléments avec les CSS (rappels)

Nous allons utiliser pour cette partie le thème sparkling.

Le CSS permet de personnaliser la présentation de son thème WordPress (voir l'animation HTML et CSS proposée par le Pic). Tous les thèmes ne permettent pas d'être personnalisées par le CSS.

Le thème sparkling que nous avons installé dans l'étape précédente le permet justement.

Pour accéder à cette configuration, on passe par l'éditeur du thème, puis CSS additionnel.

On utilise les outils développeurs de Firefox (appuyer sur F12) pour connaitre quel éléments CSS sont présents sur la page afin de le modifier via une option CSS additionnel.

Cliquer sur Sélectionner un élément de la page (icone à gauche de l'onglet Inspecteur), puis cliquer un élément de menu pour commencer.
Il est possible de faire un clic droit puis "Examiner l'élément" pour aboutir au même résultat.

On constate dans la fenêtre HTML la ligne correspondant à l'élément HTML qui affiche l'élément du menu. Observons le nom de la class CSS : menu-item

Saisir dans la zone "CSS additionnel" :

	.menu-item{
		font-weight: bold;
	}

On constate que les menus adoptent maintenant une police grasse.

Autre exemple, modifier la taille de la police du titre du site :
Faire la même opération que précédemment, on trouve la class CSS "navbar-brand".
On cherche à modifier la taille de police :

	.navbar-brand{
		font-size: 30px;
	}

On constate que la modification n'a rien fait. Si on utilise l'explorateur de Règles CSS, ont observe que la règle CSS .nav-brand à 30 px est barrée.

En effet, une règle CSS située dans le fichier style.css est prioritaire sur la notre.
Pour faire en sorte que notre règle soit prise en compte, on peut ajouter l'option !important après notre modification :

	.navbar-brand{
		font-size: 30px !important;
	}

Autre exemple, modifier l'épaisseur du texte du bouton "lire la suite":
Même procédure, la classe est more-link :

	.more-link{
		font-weight: bold;
	}

Pensez à cliquer sur le bouton "Publier" en haut, afin de valider les modifications et quelles soient visibles sur le site publique.

Nous avons vu une solution pour personnaliser rapidement le thème à condition que comme le thème Sparkling cette option soit disponible.
Une bonne pratique consiste cependant à utiliser la possibilité que nous offre WordPress par l'intermédiaire de la fonctionnalité de création d'un thème enfant.

* Cette possibilité permettra également de modifier le CSS de thèmes qui au constraire de Sparkling n'ont pas d'option de configuration des rêgles CSS.
* Autre avantage de cette facon de faire, si le thème est mis à jour, les modifications du thème étant à l'extérieur du Thème, elles ne seront pas perdues.

## 2 - Première approche de création de thèmes personnalisés avec les thèmes enfants

On utilise un moteur de recherche internet pour trouver une page dédié aux thèmes enfants :
https://developer.WordPress.org/themes/advanced-topics/child-themes/

On va y trouver des informations relatives à une arborescence de fichiers nécessaires au bon fonctionnement d'un thème enfant.

Tous les thèmes de WordPress sont situés dans le répertoire WordPress-content/themes.

On va utiliser FileZilla pour créer un nouveau répertoire pour le thème enfant, dans ce répertoire de thèmes : sparkling-child

Selon la page web relative aux thèmes enfants, il faut créer deux fichiers :
* style.css
* functions.php

Dans le premier on va copier le contenu du fichier style.css issu du thème parent Sparkling. On y ajoute une ligne Template et on change la ligne Theme Name :

	/*
	 Theme Name: Sparkling Child
	 Theme URI: http://colorlib.com/wp/themes/sparkling
	 Template : sparkling
	 Author: Colorlib
	 Author URI: http://colorlib.com/
	 Description: Sparkling is a clean minimal and responsive WordPress theme well suited for travel, health, business, finance, portfolio, design, art, photography, personal, ecommerce and any other creative websites and blogs. Developed using Bootstrap 3 that makes it mobile and tablets friendly. Theme comes with full-screen slider, social icon integration, author bio, popular posts widget and improved category widget. Sparkling incorporates latest web standards such as HTML5 and CSS3 and is SEO friendly thanks to its clean structure and codebase. It has dozens of Theme Options based on WordPress Customizer to change theme layout, colors, fonts, slider settings and much more. Theme is also translation and multilingual ready, compatible with WPML and is available in Spanish, French, Dutch, Polish, Russian, German, Brazilian Portuguese, Portuguese (Portugal), Persian (Iranian language), Romanian, Turkish, Bulgarian, Japanese, Lithuanian, Czech, Ukrainian, Traditional Chinese, Simplified Chinese, Indonesian, Estonian, Spanish (Argentina), Hungarian and Italian. Sparkling is a free WordPress theme with premium functionality and design. Theme is ecommerce ready thanks to its WooCommerce integration. Now theme is optimized to work with bbPress, Contact Form 7, Jetpack, WooCommerce and other popular free and premium plugins. Lately we introduced a sticky/fixed navigation that you can enable or disable via WordPress Customizer.
	 Version: 2.4.9
	 Tested up to: 5.9
	 Requires PHP: 5.4.0
	 License: GNU General Public License v2 or later
	 License URI: http://www.gnu.org/licenses/gpl-2.0.html
	 Text Domain: sparkling
	 Domain Path: /languages/
	 Tags: blog, news, e-commerce


	 This theme, like WordPress, is licensed under the GPL.
    
	 sparkling is based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.
       
	*/

Les options essentielles sont Theme Name et Template. Les autres informations peuvent être omises.

**Remarque** : dans l'option Template, la valeur sparkling correspond au libellé du répertoire du thème parent.

Dans le second fichier, nous allons copier les éléments suivants :

	<?php
	add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 99 ); 
	// 99 est une option en lien avec un niveau de priorité - c'est nécessaire pour que le thème parent soit pris en compte
	function my_theme_enqueue_styles() {
 
		$parent_style = 'parent-style'; 
		// This is 'twentyfifteen-style' for the Twenty Fifteen theme.
	
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'child-style',
			get_stylesheet_directory_uri() . '/style.css',
			array( $parent_style ),
			wp_get_theme()->get('Version')
    );
}
?>

Les plugins WordPress ajoutent des fonctionnalités à l'aide d'actions et de filtres, qui sont collectivement appelés hook (voir [API de plugin](https://codex.wordpress.org/Plugin_API) pour plus d'informations).

Le morceau de code proposé par la page de documentation sur les thèmes enfants met en oeuvre un hook chargé de relier un ensemble de dépences permettant finalement de relier le thème enfant avec le thème parent.

La plupart des hook sont exécutés en interne par WordPress, de sorte que le thème n'a pas besoin de balises spéciales pour fonctionner. Cependant, quelques hook doivent être inclus dans les modèles de thème. Ces hook sont déclenchés par des balises de modèle spéciales (voir [Plugin API Hooks](https://developer.wordpress.org/themes/advanced-topics/plugin-api-hooks/)).

Penser à supprimer les options CSS que nous avons mis en place dans la première partie de ce chapitre et les copier dans le fichier style.css du thème enfant :

	.menu-item{
		font-weight: bold;
	}
	.navbar-brand{
		font-size: 30px !important;
	}
	.more-link{
		font-weight: bold;
	}

On va ensuite activer le thème enfant, en utilisant la partie administration du site comme d'habitude (apparence ▸ thèmes).

On voit apparaitre le thème Sparkling Child. C'est normal qu'il n'y ait pas de visuel comme les autres thèmes ... On clique sur "Activer".

Il faudra probablement ajouter à nouveau certains widgets qui avaient éventuellement été ajouté dans le thème parent.

**Remarque :** Pour ajouter un visuel comme pour tous les thèmes, il suffit d'ajouter dans le répertoire du thème, une image qui porte le nom screenshot.png.
Attention de respecter la taille de 880x660.

## 3 - Exemples de personnalisation de quelques éléments

### Ajout d'un bouton d'action au thème

Dans la partie de personnalisation du thème : 
Personnalisation ▸ Sparkling Options ▸ Bouton d'action 
On va préciser quelques informations pour permettre d'afficher un bandeau sur la page d'accueil du site :
![Paramètres du bouton d'action](./NotesIMG/BoutonActionSparkling.png "Paramètres du bouton d'action")

Problème, ce bandeau ne s'affiche que sur la page d'accueil. Pour pouvoir l'afficher sur toutes les pages, il faut ajouter une nouvelle fonction. Voici comment ...

Ce type de bandeau s'appelle en anglais "Call for Action". Une recherche sur un moteur de recherche nous renvoie cette page : https://colorlibsupport.com/t/call-to-action-banner-on-every-page/6344

Et le code qu'il faut copier dans le fichier functions.php :

	if ( ! function_exists( 'sparkling_call_for_action' ) ) :
	/**
	* Call for action text and button displayed above content
	*/
	function sparkling_call_for_action() {
		if ( of_get_option( 'w2f_cfa_text' )!=''){
			echo '<div class="cfa">';
			echo '<div class="container">';
			echo '<div class="col-sm-8">';
			echo '<span class="cfa-text">'. of_get_option( 'w2f_cfa_text' ).'</span>';
			echo '</div>';
			echo '<div class="col-sm-4">';
			echo '<a class="btn btn-lg cfa-button" href="'. of_get_option( 'w2f_cfa_link' ). '">'. of_get_option( 'w2f_cfa_button' ). '</a>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
	}
	endif;

### Personnalisation du footer

On observe sur toutes les pages qu'une information est toujours présente dans la partie footer (le bas de page). Nous allons essayer de personnaliser cette information.

Le code de WordPress est organisé en fichiers. Un fichier est par exemple dédié au contenu du footer un autre pour le header, etc ...

Il est possible d'accéder à ces fichiers de 2 façons :

1. Par FileZilla, dans le répertoire du thème Sparkling, chercher le fichier footer.php, et le télécharger sur son poste de travail

2. En utilisant l'éditeur de code de WordPress : Apparence ▸ Editeur
	
	On sélectionne ensuite le thème que l'on souhaite modifier puis le fichier footer.php.

Le thème Sparkling est situé à l'emplacement suivant : 

	/www/wp-content/themes/sparkling

On cherche à modifier le fichier du thème enfant (pour rendre les modifications persistantes). On ouvre donc le fichier téléchargé par Filezilla avec un éditeur (de préférence de code comme sublime text, brackets, visual studio code ou atom). Avec un éditeur de code il est alors possible de visualiser plus facilement le code car il est coloré.

En utilisant l'inspecteur des outils de développement du navigateur, on cherche la DIV qui correspond à la partie affichant le Copyright. Si on observe le fichier footer.php, on observe qu'il y a une partie qui semble correspondre à notre objectif :

	<div class="copyright col-md-6">
		<?php echo of_get_option( 'custom_footer_text', 'sparkling' ); ?>
		<?php sparkling_footer_info(); ?>
	</div>

Pour l'exercice on va tout simplement supprimer cette DIV. Attention à bien enlever tout le bloc en intégrant la balise ouvrante <div>, jusqu'à la balise fermante </div>.

Remarquez au passage la transformation que le code PHP produit avec la version HTML que nous montre l'outil pour developpeur du navigateur.

Nous allons maintenant faire en sorte de ne pas toucher au thème d'origine Sprakling mais modifier son thème enfant.

Il suffit donc de copier le fichier footer.php modifié dans le dossier de thème enfant :

	/www/wp-content/themes/sparkling-child

Observer le résultat sur le site. On constate que lorsque on adapte un fichier provenant d'un thème parent dans un thème enfant, c'est de dernier qui devient prioritaire. Le thème enfant est privilégié.

# Notes pour plus tard

WordPress utilise depuis Janvier 2022 une nouvelle facon de mettre en page le thème grace à l'extension de Gutenberg à l'ensemble du site.
Quelques liens :
* https://www.blogdumoderateur.com/wordpress-full-site-editing-evolution-cms/
* https://fr.wordpress.org/2022/01/25/wordpress-5-9/#more-2330
* https://fr.wordpress.org/support/article/block-themes/
* https://fr.wordpress.org/support/article/site-editor/
* https://make.wordpress.org/core/2022/01/04/block-themes-a-new-way-to-build-themes-in-wordpress-5-9/

