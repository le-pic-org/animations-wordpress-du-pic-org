<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 99 ); // 99 est une option en lien avec un niveau de priorité - c'est nécessaire pour que le thème parent soit pris en compte
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

if ( ! function_exists( 'sparkling_call_for_action' ) ) :
    /**
     * Call for action text and button displayed above content
     */
    function sparkling_call_for_action() {
      if ( of_get_option( 'w2f_cfa_text' )!=''){
        echo '<div class="cfa">';
          echo '<div class="container">';
            echo '<div class="col-sm-8">';
              echo '<span class="cfa-text">'. of_get_option( 'w2f_cfa_text' ).'</span>';
              echo '</div>';
              echo '<div class="col-sm-4">';
              echo '<a class="btn btn-lg cfa-button" href="'. of_get_option( 'w2f_cfa_link' ). '">'. of_get_option( 'w2f_cfa_button' ). '</a>';
              echo '</div>';
          echo '</div>';
        echo '</div>';
      }
    }
    endif;
    

?>