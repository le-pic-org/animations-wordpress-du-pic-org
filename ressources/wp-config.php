<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wp_st1');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'wp-st1');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'qwerty123');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&sa9pu$hDGLT~<TSC_sRk34o&z.t*Eio2lk|MJdw,QWoc8gTnPm)( 5&&)EH(keK');
define('SECURE_AUTH_KEY',  '{4KDo$L{jGd^wk[gR: 8OP^HKKo2v8#hgXWXt9Ltu_C_AhMf}[@z?tZWeBI^k:o1');
define('LOGGED_IN_KEY',    '<^_Mx+{U(/#xb~l^+]$=uu%_uCr]i]Ljrnnj&aCo@&&&zN}|3(2zR]!J7>z6TJ^i');
define('NONCE_KEY',        'u|oE O<.Gelx9+c96-hx]O?OVS>$OmzA(Q<lf9w{hc6j#TfeND vQFf$wrrsO1e8');
define('AUTH_SALT',        'oWL5j$aEgZ5nPQN~ErzOzI`ZGyf1)NLIuCTU340WEGO*$6}(6CZEo80SF[wkeGJp');
define('SECURE_AUTH_SALT', '*G=hC~UW|*JeI^~%VY-b.0q&<*DMcTMZ%m38ixPqSo->o83qqy>fG$jLf^^c_4qR');
define('LOGGED_IN_SALT',   '{F(<{A48l#d>g^ -80OudB%T>j^?0HE}tsJcH.dQN(;a`fFTl/uA=7aXD[6)82:D');
define('NONCE_SALT',       'Mu]A(BcH|/ECPHn (a+Q|LXzd2MJ/%d>9Nf3pt,uL72;3fB!m>5zIZ4uTo);V)kq');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/** Configuration du plugin ssh-sftp-updater-support */
define('FS_METHOD', 'ssh2');

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');